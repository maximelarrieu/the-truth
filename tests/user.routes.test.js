
const request = require('supertest');
const app = require("../app");
const db = require('../models')

describe('Users routes', function() {
    var adminAuth = {};
    var userAuth ={};
    beforeAll(async () => {
        await db.sequelize.sync()
      })
    // afterAll(async () => {
    //     await db.sequelize.close();
    //    });
    
      it('should get the admin token', async(done) => {
        request(app)
            .post('/api/users/login')
            .set({'Accept':'application/json'})
            .send({
                username: 'maximelarrieu',
                password: 'password'
            })
            .expect(200)
            .end((err, res) => {
              if (err) {
                console.log(err)
                  done.fail(err);
              } else {
                adminAuth.token = res.body.user.accessToken;
                console.log(adminAuth)
                return done();
              }
          });
      });
    
    it('should create a user', async(done) => {
      request(app)
        .post('/api/users/register')
        .set({'Accept':'application/json'})
        .send({
            "username":"userTest",
            "email":"usertest@yahoo.us",
            "password":"12345"
        })
        .expect(200)
        .end((err, res) => {
            if (err) {
             done.fail(err);
            } else {
                 
              done();
            }
        });
    });
    it('should fail creating a user because the username is already use', async(done) => {
      request(app)
        .post('/api/users/register')
        .set({'Accept':'application/json'})
        .send({
            "username":"userTest",
            "email":"usertest@yahoo.us",
            "password":"12345"
        })
        .expect(400)
        .end((err, res) => {
            if (err) {
             done.fail(err);
            } else {
                 
              done();
            }
        });
    });
    it('should fail creating a user because some field are missing', async(done) => {
      request(app)
        .post('/api/users/register')
        .set({'Accept':'application/json'})
        .send({
            "username":"userTest",
            "email":"usertest@yahoo.us",
            "password":"12345"
        })
        .expect(400)
        .end((err, res) => {
            if (err) {
             done.fail(err);
            } else {
                 
              done();
            }
        });
    });
    it('should log the user', async(done) => {
      request(app)
        .post('/api/users/login')
        .set({'Accept':'application/json'})
        .send({
            "username":"userTest",
            "password":"12345"
        })
        .expect(200)
        .end((err, res) => {
            if (err) {
             done.fail(err);
            } else {
                userAuth.token = res.body.user.accessToken;
                userAuth.id = res.body.user.id;
              done();
            }
        });
    });
    it('should fail login the user, because of bad credentials', async(done) => {
      request(app)
        .post('/api/users/login')
        .set({'Accept':'application/json'})
        .send({
            "username":"userTest",
            "password":"dcczdcd"
        })
        .expect(401)
        .end((err, res) => {
            if (err) {
             done.fail(err);
            } else {
                 
              done();
            }
        });
    });
    it('should fail login the user because some fields are missing', async(done) => {
      request(app)
        .post('/api/users/login')
        .set({'Accept':'application/json'})
        .send({
            "username":"userTest"
        })
        .expect(500)
        .end((err, res) => {
            if (err) {
             done.fail(err);
            } else {
                 
              done();
            }
        });
    });
    it('should get all users', async(done) => {
      request(app)
        .get('/api/users')
        .set({'Authorization': adminAuth.token,'Accept':'application/json'})
        .expect(200)
        .end((err, res) => {
            if (err) {
            done.fail(err);
            } else {
                 
              done();
            }
        });
    });
    it('should not get all users because of bad token', async(done) => {
      request(app)
        .get('/api/users')
        .set({'Authorization': 'vlkkfeuihfhzufozh','Accept':'application/json'})
        .expect(401)
        .end((err, res) => {
            if (err) {
            done.fail(err);
            } else {
                 
              done();
            }
        });
    });
    it('should not get all users because there is no token', async(done) => {
      request(app)
        .get('/api/users')
        .set({'Accept':'application/json'})
        .expect(500)
        .end((err, res) => {
            if (err) {
            done.fail(err);
            } else {
                 
              done();
            }
        });
    });
    it('should get a user by it\'s id', async(done) => {
      request(app)
        .get('/api/users/1')
        .set({'Authorization': adminAuth.token,'Accept':'application/json'})
        .expect(200)
        .end((err, res) => {
            if (err) {
            done.fail(err);
            } else {
                 
              done();
            }
        });
    });
    it('should not get a user by it\'s id because the id doesn\'t exist', async(done) => {
      request(app)
        .get('/api/users/233442424')
        .set({'Authorization': adminAuth.token,'Accept':'application/json'})
        .expect(500)
        .end((err, res) => {
            if (err) {
            done.fail(err);
            } else {
                 
              done();
            }
        });
    });
    it('should modify a user by it\'s id ', async(done) => {
      request(app)
        .put('/api/users/' + userAuth.id)
        .set({'Authorization': adminAuth.token,'Accept':'application/json'})
        .send({
          "username":"userModifiedusername",
          "email":"userModifiedusername@yahoo.us",
          "password":"password"
      })
        .expect(200)
        .end((err, res) => {
            if (err) {
            done.fail(err);
            } else {
                 
              done();
            }
        });
    });
    it('should not modify a user by it\'s id because not admin', async(done) => {
      request(app)
        .put('/api/users/3')
        .set({'Authorization': userAuth.token,'Accept':'application/json'})
        .send({
          "username":"userModifiedusername",
          "email":"userModifiedusername@yahoo.us",
          "password":"passwordModified"
      })
        .expect(403)
        .end((err, res) => {
            if (err) {
            done.fail(err);
            } else {
                 
              done();
            }
        });
    });
    it('should  modify a user\'s roles by it\'s id because admin', async(done) => {
      request(app)
        .put('/api/users/' + userAuth.id + '/role')
        .set({'Authorization': adminAuth.token,'Accept':'application/json'})
        .send({
          "role": 2
        })
        .expect(200)
        .end((err, res) => {
            if (err) {
            done.fail(err);
            } else {
                 
              done();
            }
        });
    });
    it('should not modify a user\'s roles by it\'s id because not admin', async(done) => {
      request(app)
        .put('/api/users/3/role')
        .set({'Authorization': userAuth.token,'Accept':'application/json'})
        .send({
          "roles": 3
      })
        .expect(403)
        .end((err, res) => {
            if (err) {
            done.fail(err);
            } else {
                 
              done();
            }
        });
    });
    it('should not delete a user by it\'s id because not admin', async(done) => {
      request(app)
        .delete('/api/users/3')
        .set({'Authorization': userAuth.token,'Accept':'application/json'})
        .expect(403)
        .end((err, res) => {
            if (err) {
            done.fail(err);
            } else {
              done();
            }
        });
    });
    it('should delete a user by it\'s id because admin', async(done) => {
      request(app)
        .delete('/api/users/'+ userAuth.id)
        .set({'Authorization': adminAuth.token,'Accept':'application/json'})
        .expect(200)
        .end((err, res) => {
            if (err) {
            done.fail(err);
            } else {
              done();
            }
        });
    });
  });
