
const request = require('supertest');
const app = require("../app");
const db = require('../models')

describe('topics routes', function() {
    var adminAuth = {};
    var moderatorAuth = {};
    var visitorAuth = {};
    var topic = {};
    beforeAll(async () => {
        await db.sequelize.sync()
      })
    
      it('for an admin token', async(done) => {
        request(app)
            .post('/api/users/login')
            .set({'Accept':'application/json'})
            .send({
                username: 'maximelarrieu',
                password: 'password'
            })
            .expect(200)
            .end((err, res) => {
              if (err) {
                console.log(err)
                  done.fail(err);
              } else {
                adminAuth.token = res.body.user.accessToken;
                console.log(adminAuth)
                return done();
              }
          });
      });
    
    it('for a visitor token', async(done) => {
      request(app)
        .post('/api/users/login')
        .set({'Accept':'application/json'})
        .send({
            "username":"xanders",
            "password":"password"
        })
        .expect(200)
        .end((err, res) => {
            if (err) {
             done.fail(err);
            } else {
                visitorAuth.token = res.body.user.accessToken;
                visitorAuth.id = res.body.user.id;
              done();
            }
        });
    });
    it('for a moderator token', async(done) => {
      request(app)
        .post('/api/users/login')
        .set({'Accept':'application/json'})
        .send({
            "username":"tombiato",
            "password":"password"
        })
        .expect(200)
        .end((err, res) => {
            if (err) {
             done.fail(err);
            } else {
                moderatorAuth.token = res.body.user.accessToken;
                moderatorAuth.id = res.body.user.id;
              done();
            }
        });
    });
    
    it('should get all topics', async(done) => {
      request(app)
        .get('/api/topics')
        .set({'Authorization': adminAuth.token,'Accept':'application/json'})
        .expect(200)
        .end((err, res) => {
            if (err) {
            done.fail(err);
            } else {
                 
              done();
            }
        });
    });
    it('should not get all topics because no token', async(done) => {
      request(app)
        .get('/api/topics')
        .set({'Authorization': 'bad token','Accept':'application/json'})
        .expect((401))
        .end((err, res) => {
            if (err) {
            done.fail(err);
            } else {
              done();
            }
        });
    });
    it('should create a topic', async(done) => {
      request(app)
        .post('/api/topics')
        .set({'Authorization': adminAuth.token,'Accept':'application/json'})
        .send({
          "name": "test topic",
          "category": 2
        })
        .expect(200)
        .end((err, res) => {
            if (err) {
             done.fail(err);
            } else {
              topic = res.body.topic
              done();
            }
        });
    });
    it('should not create a topic because no body provided', async(done) => {
      request(app)
        .post('/api/topics')
        .set({'Authorization': adminAuth.token,'Accept':'application/json'})
        .expect(400)
        .end((err, res) => {
            if (err) {
             done.fail(err);
            } else {
              done();
            }
        });
    });
    it('should not create a topic because not admin', async(done) => {
      request(app)
        .post('/api/topics')
        .set({'Authorization': visitorAuth.token,'Accept':'application/json'})
        .send({
          "name": "topic tha wont exist"
        })
        .expect(403)
        .end((err, res) => {
            if (err) {
             done.fail(err);
            } else {
              done();
            }
        });
    });
    
    it('should get a topic by it\'s id', async(done) => {
      request(app)
        .get('/api/topics/' + topic.id)
        .set({'Authorization': adminAuth.token,'Accept':'application/json'})
        .expect(200)
        .end((err, res) => {
            if (err) {
            done.fail(err);
            } else {
                 
              done();
            }
        });
    });
    it('should modify a topic by it\'s id', async(done) => {
      request(app)
        .put('/api/topics/' + topic.id )
        .set({'Authorization': adminAuth.token,'Accept':'application/json'})
        .send({
          "name": "modified test topic"
        })
        .expect(200)
        .end((err, res) => {
            if (err) {
            done.fail(err);
            } else {
              done();
            }
        });
    });
    it('should not modify a topic by it\'s id because not admin', async(done) => {
      request(app)
        .put('/api/topics/' + topic.id )
        .set({'Authorization': visitorAuth.token,'Accept':'application/json'})
        .send({
          "name": "modifiedRoleTest"
        })
        .expect(403)
        .end((err, res) => {
            if (err) {
            done.fail(err);
            } else {
              done();
            }
        });
    });
    it('should not modify a topic by it\'s id because no token', async(done) => {
      request(app)
        .put('/api/topics/' + topic.id )
        .set({'Accept':'application/json'})
        .send({
          "name": "Reptilians"
        })
        .expect(500)
        .end((err, res) => {
            if (err) {
            done.fail(err);
            } else {
              done();
            }
        });
    });
    it('should not delete a topic by it\'s id because not admin', async(done) => {
      request(app)
        .delete('/api/topics/'+ topic.id)
        .set({'Authorization': visitorAuth.token,'Accept':'application/json'})
        .expect(403)
        .end((err, res) => {
            if (err) {
            done.fail(err);
            } else {
              done();
            }
        });
    });
    it('should delete a topic by it\'s id because admin', async(done) => {
      request(app)
        .delete('/api/topics/'+ topic.id)
        .set({'Authorization': adminAuth.token,'Accept':'application/json'})
        .expect(200)
        .end((err, res) => {
            if (err) {
            done.fail(err);
            } else {
              done();
            }
        });
    });
  });

