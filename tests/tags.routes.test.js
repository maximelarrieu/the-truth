
const request = require('supertest');
const app = require("../app");
const db = require('../models')

describe('tags routes', function() {
    var adminAuth = {};
    var moderatorAuth = {};
    var visitorAuth = {};
    var tag = {};
    beforeAll(async () => {
        await db.sequelize.sync()
      })
    
      it('for an admin token', async(done) => {
        request(app)
            .post('/api/users/login')
            .set({'Accept':'application/json'})
            .send({
                username: 'maximelarrieu',
                password: 'password'
            })
            .expect(200)
            .end((err, res) => {
              if (err) {
                console.log(err)
                  done.fail(err);
              } else {
                adminAuth.token = res.body.user.accessToken;
                console.log(adminAuth)
                return done();
              }
          });
      });
    
    it('for a visitor token', async(done) => {
      request(app)
        .post('/api/users/login')
        .set({'Accept':'application/json'})
        .send({
            "username":"xanders",
            "password":"password"
        })
        .expect(200)
        .end((err, res) => {
            if (err) {
             done.fail(err);
            } else {
                visitorAuth.token = res.body.user.accessToken;
                visitorAuth.id = res.body.user.id;
              done();
            }
        });
    });
    it('for a moderator token', async(done) => {
      request(app)
        .post('/api/users/login')
        .set({'Accept':'application/json'})
        .send({
            "username":"tombiato",
            "password":"password"
        })
        .expect(200)
        .end((err, res) => {
            if (err) {
             done.fail(err);
            } else {
                moderatorAuth.token = res.body.user.accessToken;
                moderatorAuth.id = res.body.user.id;
              done();
            }
        });
    });
    
    it('should get all tags', async(done) => {
      request(app)
        .get('/api/tags')
        .set({'Authorization': adminAuth.token,'Accept':'application/json'})
        .expect(200)
        .end((err, res) => {
            if (err) {
            done.fail(err);
            } else {
                 
              done();
            }
        });
    });
    it('should not get all tags because no token', async(done) => {
      request(app)
        .get('/api/tags')
        .set({'Authorization': 'bad token','Accept':'application/json'})
        .expect((401))
        .end((err, res) => {
            if (err) {
            done.fail(err);
            } else {
              done();
            }
        });
    });
    it('should create a tag', async(done) => {
      request(app)
        .post('/api/tags')
        .set({'Authorization': adminAuth.token,'Accept':'application/json'})
        .send({
          "name": "test tag"
        })
        .expect(200)
        .end((err, res) => {
            if (err) {
             done.fail(err);
            } else {
              tag = res.body.tag
              done();
            }
        });
    });
    it('should not create a tag because no body provided', async(done) => {
      request(app)
        .post('/api/tags')
        .set({'Authorization': adminAuth.token,'Accept':'application/json'})
        .expect(400)
        .end((err, res) => {
            if (err) {
             done.fail(err);
            } else {
              done();
            }
        });
    });
    it('should not create a tag because not admin', async(done) => {
      request(app)
        .post('/api/tags')
        .set({'Authorization': visitorAuth.token,'Accept':'application/json'})
        .send({
          "name": "tag tha wont exist"
        })
        .expect(403)
        .end((err, res) => {
            if (err) {
             done.fail(err);
            } else {
              done();
            }
        });
    });
    
    it('should get a tag by it\'s id', async(done) => {
      request(app)
        .get('/api/tags/' + tag.id)
        .set({'Authorization': adminAuth.token,'Accept':'application/json'})
        .expect(200)
        .end((err, res) => {
            if (err) {
            done.fail(err);
            } else {
                 
              done();
            }
        });
    });
    it('should modify a tag by it\'s id', async(done) => {
      request(app)
        .put('/api/tags/' + tag.id )
        .set({'Authorization': adminAuth.token,'Accept':'application/json'})
        .send({
          "name": "modified test tag"
        })
        .expect(200)
        .end((err, res) => {
            if (err) {
            done.fail(err);
            } else {
              done();
            }
        });
    });
    it('should not modify a tag by it\'s id because not admin', async(done) => {
      request(app)
        .put('/api/tags/' + tag.id )
        .set({'Authorization': visitorAuth.token,'Accept':'application/json'})
        .send({
          "name": "modifiedRoleTest"
        })
        .expect(403)
        .end((err, res) => {
            if (err) {
            done.fail(err);
            } else {
              done();
            }
        });
    });
    it('should not modify a tag by it\'s id because no token', async(done) => {
      request(app)
        .put('/api/tags/' + tag.id )
        .set({'Accept':'application/json'})
        .send({
          "name": "Reptilians"
        })
        .expect(500)
        .end((err, res) => {
            if (err) {
            done.fail(err);
            } else {
              done();
            }
        });
    });
    it('should not delete a tag by it\'s id because not admin', async(done) => {
      request(app)
        .delete('/api/tags/'+ tag.id)
        .set({'Authorization': visitorAuth.token,'Accept':'application/json'})
        .expect(403)
        .end((err, res) => {
            if (err) {
            done.fail(err);
            } else {
              done();
            }
        });
    });
    it('should delete a tag by it\'s id because admin', async(done) => {
      request(app)
        .delete('/api/tags/'+ tag.id)
        .set({'Authorization': adminAuth.token,'Accept':'application/json'})
        .expect(200)
        .end((err, res) => {
            if (err) {
            done.fail(err);
            } else {
              done();
            }
        });
    });
  });

