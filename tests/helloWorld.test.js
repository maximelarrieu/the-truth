const helloWorld = require("../helloWorld");

test('hello there plus name', () => {
    expect(helloWorld('you')).toBe('hello there you');
  });