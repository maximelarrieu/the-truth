
const request = require('supertest');
const app = require("../app");
const db = require('../models')

describe('Categories routes', function() {
    var adminAuth = {};
    var moderatorAuth = {};
    var visitorAuth = {};
    var category = {};
    beforeAll(async () => {
        await db.sequelize.sync()
      })
    
      it('for an admin token', async(done) => {
        request(app)
            .post('/api/users/login')
            .set({'Accept':'application/json'})
            .send({
                username: 'maximelarrieu',
                password: 'password'
            })
            .expect(200)
            .end((err, res) => {
              if (err) {
                console.log(err)
                  done.fail(err);
              } else {
                adminAuth.token = res.body.user.accessToken;
                console.log(adminAuth)
                return done();
              }
          });
      });
    
    it('for a visitor token', async(done) => {
      request(app)
        .post('/api/users/login')
        .set({'Accept':'application/json'})
        .send({
            "username":"xanders",
            "password":"password"
        })
        .expect(200)
        .end((err, res) => {
            if (err) {
             done.fail(err);
            } else {
                visitorAuth.token = res.body.user.accessToken;
                visitorAuth.id = res.body.user.id;
              done();
            }
        });
    });
    it('for a moderator token', async(done) => {
      request(app)
        .post('/api/users/login')
        .set({'Accept':'application/json'})
        .send({
            "username":"tombiato",
            "password":"password"
        })
        .expect(200)
        .end((err, res) => {
            if (err) {
             done.fail(err);
            } else {
                moderatorAuth.token = res.body.user.accessToken;
                moderatorAuth.id = res.body.user.id;
              done();
            }
        });
    });
    
    it('should get all categories', async(done) => {
      request(app)
        .get('/api/categories')
        .set({'Authorization': adminAuth.token,'Accept':'application/json'})
        .expect(200)
        .end((err, res) => {
            if (err) {
            done.fail(err);
            } else {
                 
              done();
            }
        });
    });
    it('should not get all categories because no token', async(done) => {
      request(app)
        .get('/api/categories')
        .set({'Authorization': 'bad token','Accept':'application/json'})
        .expect((401))
        .end((err, res) => {
            if (err) {
            done.fail(err);
            } else {
              done();
            }
        });
    });
    it('should create a category', async(done) => {
      request(app)
        .post('/api/categories')
        .set({'Authorization': adminAuth.token,'Accept':'application/json'})
        .send({
          "name": "Iluminati"
        })
        .expect(200)
        .end((err, res) => {
            if (err) {
             done.fail(err);
            } else {
              category = res.body.category
              done();
            }
        });
    });
    it('should not create a category because no body provided', async(done) => {
      request(app)
        .post('/api/categories')
        .set({'Authorization': adminAuth.token,'Accept':'application/json'})
        .expect(400)
        .end((err, res) => {
            if (err) {
             done.fail(err);
            } else {
              done();
            }
        });
    });
    it('should not create a category because no token provided', async(done) => {
      request(app)
        .post('/api/categories')
        .set({'Accept':'application/json'})
        .send({
          "name": "Iluminati"
        })
        .expect(500)
        .end((err, res) => {
            if (err) {
             done.fail(err);
            } else {
              done();
            }
        });
    });
    
    it('should get a categorie by it\'s id', async(done) => {
      request(app)
        .get('/api/categories/' + category.id)
        .set({'Authorization': adminAuth.token,'Accept':'application/json'})
        .expect(200)
        .end((err, res) => {
            if (err) {
            done.fail(err);
            } else {
                 
              done();
            }
        });
    });
    it('should modify a category by it\'s id', async(done) => {
      request(app)
        .put('/api/categories/' + category.id )
        .set({'Authorization': adminAuth.token,'Accept':'application/json'})
        .send({
          "name": "Reptilians"
        })
        .expect(200)
        .end((err, res) => {
            if (err) {
            done.fail(err);
            } else {
              done();
            }
        });
    });
    it('should not modify a category by it\'s id because no token', async(done) => {
      request(app)
        .put('/api/categories/' + category.id )
        .set({'Accept':'application/json'})
        .send({
          "name": "Reptilians"
        })
        .expect(500)
        .end((err, res) => {
            if (err) {
            done.fail(err);
            } else {
              done();
            }
        });
    });
    it('should not delete a user by it\'s id because not admin', async(done) => {
      request(app)
        .delete('/api/categories/'+ category.id)
        .set({'Authorization': visitorAuth.token,'Accept':'application/json'})
        .expect(403)
        .end((err, res) => {
            if (err) {
            done.fail(err);
            } else {
              done();
            }
        });
    });
    it('should delete a category by it\'s id because admin', async(done) => {
      request(app)
        .delete('/api/categories/'+ category.id)
        .set({'Authorization': adminAuth.token,'Accept':'application/json'})
        .expect(200)
        .end((err, res) => {
            if (err) {
            done.fail(err);
            } else {
              done();
            }
        });
    });
  });
