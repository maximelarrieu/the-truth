
const request = require('supertest');
const app = require("../app");
const db = require('../models')

describe('comments routes', function() {
  var adminAuth = {};
  var moderatorAuth = {};
  var visitorAuth = {};
  var comment = {};
  beforeAll(async () => {
      await db.sequelize.sync()
    })
  
    it('for an admin token', async(done) => {
      request(app)
          .post('/api/users/login')
          .set({'Accept':'application/json'})
          .send({
              username: 'maximelarrieu',
              password: 'password'
          })
          .expect(200)
          .end((err, res) => {
            if (err) {
              console.log(err)
                done.fail(err);
            } else {
              adminAuth.token = res.body.user.accessToken;
              console.log(adminAuth)
              return done();
            }
        });
    });
  
  it('for a visitor token', async(done) => {
    request(app)
      .post('/api/users/login')
      .set({'Accept':'application/json'})
      .send({
          "username":"xanders",
          "password":"password"
      })
      .expect(200)
      .end((err, res) => {
          if (err) {
           done.fail(err);
          } else {
              visitorAuth.token = res.body.user.accessToken;
              visitorAuth.id = res.body.user.id;
            done();
          }
      });
  });
  it('for a moderator token', async(done) => {
    request(app)
      .post('/api/users/login')
      .set({'Accept':'application/json'})
      .send({
          "username":"tombiato",
          "password":"password"
      })
      .expect(200)
      .end((err, res) => {
          if (err) {
           done.fail(err);
          } else {
              moderatorAuth.token = res.body.user.accessToken;
              moderatorAuth.id = res.body.user.id;
            done();
          }
      });
  });
  
  it('should get all comments of the post', async(done) => {
    request(app)
      .get('/api/comments')
      .set({'Authorization': visitorAuth.token,'Accept':'application/json'})
      .query({
        post:"1", page:"1", size:"1"
      })
      .expect(200)
      .end((err, res) => {
          if (err) {
          done.fail(err);
          } else {
               
            done();
          }
      });
  });
  it('should not get all comments because no token', async(done) => {
    request(app)
      .get('/api/comments')
      .set({'Authorization': 'bad token','Accept':'application/json'})
      .expect((401))
      .end((err, res) => {
          if (err) {
          done.fail(err);
          } else {
            done();
          }
      });
  });
  it('should create a comments', async(done) => {
    request(app)
      .post('/api/comments')
      .set({'Authorization': adminAuth.token,'Accept':'application/json'})
      .send({
        "author": 1,
        "content": "Aliens claim that the earth is round",
        "post": 1
      })
      .expect(200)
      .end((err, res) => {
          if (err) {
           done.fail(err);
          } else {
            comment = res.body.comment
            done();
          }
      });
  });
  it('should not create a comment because no body provided', async(done) => {
    request(app)
      .post('/api/comments')
      .set({'Authorization': adminAuth.token,'Accept':'application/json'})
      .expect(400)
      .end((err, res) => {
          if (err) {
           done.fail(err);
          } else {
            done();
          }
      });
  });
  it('should not create a comment because no token provided', async(done) => {
    request(app)
      .post('/api/comments')
      .set({'Accept':'application/json'})
      .send({
        "author": 1,
        "content": "Aliens claim that the earth is round",
        "post": 1
      })
      .expect(500)
      .end((err, res) => {
          if (err) {
           done.fail(err);
          } else {
            done();
          }
      });
  });
  
  it('should get a comment by it\'s id', async(done) => {
    request(app)
      .get('/api/comments/' + comment.id)
      .set({'Authorization': adminAuth.token,'Accept':'application/json'})
      .expect(200)
      .end((err, res) => {
          if (err) {
          done.fail(err);
          } else {
               
            done();
          }
      });
  });
  it('should modify a comment by it\'s id', async(done) => {
    request(app)
      .put('/api/comments/' + comment.id )
      .set({'Authorization': adminAuth.token,'Accept':'application/json'})
      .send({
        "author": 1,
        "content": "Brigitte Macron is a man",
        "post": 1
      })
      .expect(200)
      .end((err, res) => {
          if (err) {
          done.fail(err);
          } else {
            done();
          }
      });
  });
  it('should not modify a category by it\'s id because not the author', async(done) => {
    request(app)
      .put('/api/comments/' + comment.id )
      .set({'Authorization': visitorAuth.token,'Accept':'application/json'})
      .send({
        "author": 1,
        "content": "yo here is a cool comment !",
        "post": 1
      })
      .expect(403)
      .end((err, res) => {
          if (err) {
          done.fail(err);
          } else {
            done();
          }
      });
  });
  it('should not delete a comment by it\'s id because not author', async(done) => {
    request(app)
      .delete('/api/comments/'+ comment.id)
      .set({'Authorization': visitorAuth.token,'Accept':'application/json'})
      .expect(403)
      .end((err, res) => {
          if (err) {
          done.fail(err);
          } else {
            done();
          }
      });
  });
  it('should delete a comment by it\'s id because author', async(done) => {
    request(app)
      .delete('/api/comments/'+ comment.id)
      .set({'Authorization': adminAuth.token,'Accept':'application/json'})
      .expect(200)
      .end((err, res) => {
          if (err) {
          done.fail(err);
          } else {
            done();
          }
      });
  });
});
