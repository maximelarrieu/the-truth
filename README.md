# the-truth

API d'un forum complotiste afin de découvrir la vérité sur notre monde. Ouvrez les yeux !

## Getting started
### Serveur local

Cloner ou merger sur sa branche.
Lancez la commande : `npm install`.

#### Initialiser la base de données
+ Lancez le script shell `RUN.sh` (en le rendant exécutable au préalable).
> Ce script permet de créer la base de données, ses tables et relations ainsi que de lancer le serveur.

+ Lancez le second script shell (dans un autre terminal) `SEED.sh` qui permet de seeder la table (One-To-Many) `posts_tags` ayant besoin que le serveur soit déjà lancé pour être créée.

### Dockerizé
Tout d'abord, il faut changer la configuration du fichier `config/config.json` de cette façon :
```
{
    "username": "root",
    "password": "root",
    "database": "the_truth",
    "host": "docker-mariadb",
    "port": "3306",
    "dialect": "mysql"
}
```

Puis créez l'image du serveur nodeJS.
+ Lancez la commande : `docker build -t the-truth .`.

Démarrez le docker-compose qui créera les containers du serveur node et du serveur mariadb.
+ Lancez la commande : `docker-compose up`.

Le serveur est maintenant dockerisé et accessible depuis `http://localhost:3000`.

### Documentation
Découvrez les routes de l'API à l'adresse suivante : `http://localhost:3000/documentation/`.

### Tests 
Pour tester l'API :
+ Lancez la commande : `npm test`.
Pour voir la couverture de test de l'API :
+ Lancez la commande : `npm test-coverage`.
pour avoir un affichage plus sympa de la couverture de test, ouvrez le fichier suivant dans votre navigateur "../coverage\lcov-report\index.html"
le rapport html devrait ressembler à ça :
![What is this](./coverage-exemple.png)

### SonarQube
Pour executer SonarQube sue le projet, vous devez d'abord le télécharger sur `https://www.sonarqube.org/downloads/` et l'installer localement.

Lancer le script de démarage `sonarqube/bin/[OS]/sonar.sh console` ouvrira SonarQube sur le port 9000 et vous permettra de créer un compte local.

Vous pourez ensuite analyser le projet en suivant les quelques instructions suivantes :
+ Cliquez sur `Create new project`
+ Choisissez un nom pour votre projet et votre clé
+ Sous `Provide a token` cliquez sur `Generate a token` puis sur `Continue`
+ Séléctionnez le language de votre projet et executez les commandes qui vous sont proposées en CLI directement à l'interieur du dossier de votre projet, la page de SonarQube va alors changer unee fois l'analyse terminé et afficher les résultats