let bcrypt = require("bcryptjs");

'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    // await queryInterface.bulkInsert('Roles', [ 
    //   { name: 'visitor', createdAt: new Date(), updatedAt: new Date() },
    //   { name: 'moderator', createdAt: new Date(), updatedAt: new Date() },
    //   { name: 'administrator', createdAt: new Date(), updatedAt: new Date() }
      
    // ], {});

    const roleAdmin = await queryInterface.sequelize.query(
      `SELECT id FROM Roles WHERE name LIKE 'administrator'`
    );
    const roleUser = await queryInterface.sequelize.query(
      `SELECT id FROM Roles WHERE name LIKE 'visitor'`
    );
    const roleModertor = await queryInterface.sequelize.query(
      `SELECT id FROM Roles WHERE name LIKE 'moderator'`
    );
    const rolesAdmin = roleAdmin[0]
    const rolesVisitor = roleUser[0]
    const rolesModerator = roleModertor[0]


    return await queryInterface.bulkInsert('Users', [
      {
        username: 'maximelarrieu',
        email: 'maxime.larrieu@ynov.com',
        password: bcrypt.hashSync('password'),
        role: rolesAdmin[0].id, 
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        username: 'tombiato',
        email: 'tom.biato@ynov.com',
        password: bcrypt.hashSync('password'),
        role: rolesModerator[0].id,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        username: 'xanders',
        email: 'xanders@ynov.com',
        password: bcrypt.hashSync('password'),
        role: rolesVisitor[0].id,
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ])
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    return queryInterface.bulkDelete('Users', null, {});
  }
};
