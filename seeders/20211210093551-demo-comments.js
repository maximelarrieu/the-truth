'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    const users = await queryInterface.sequelize.query(
      `SELECT id FROM Users`
    );
    const usersRow = users[0];

    const posts = await queryInterface.sequelize.query(
      `SELECT id FROM Posts`
    );
    const postsRow = posts[0];

    await queryInterface.bulkInsert('Comments', [
      {
        author: usersRow[2].id,
        content: `L'état nous manipule! C'est clair! #jenesuispasunmouton`,
        post: postsRow[0].id,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        author: usersRow[2].id,
        content: 'ok boomer',
        post: postsRow[1].id,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        author: usersRow[1].id,
        content: 'je suis sûr que l\'on nous ment! j`irai moi-même voir cela!',
        post: postsRow[1].id,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        author: usersRow[0].id,
        content: 'Je capte déjà la 5g grâce à mon galaxy note 7 :)',
        post: postsRow[2].id,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        author: usersRow[2].id,
        content: 'Idem pour moi, très gros mal de crâne depuis cette 32ème dose mais toujours pas de 5g....dommage....',
        post: postsRow[2].id,
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ], {}) 
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Comments', null, {});
  }
};
