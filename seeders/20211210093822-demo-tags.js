'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('Tags', [
      {
        name: 'coronavirus',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: '5g',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'arcachon',
        createdAt: new Date(),
        updatedAt: new Date()
      },{
        name: 'mensonges',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'pass sanitaire',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'bullshit',
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ])
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Tags', null, {})
  }
};
