'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    const posts = await queryInterface.sequelize.query(
      `SELECT id FROM Posts`
    );
    const postsRow = posts[0];

    const tags = await queryInterface.sequelize.query(
      `SELECT id FROM Tags`
    );
    const tagsRow = tags[0];

    await queryInterface.bulkInsert('posts_tags', [
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        PostId: postsRow[0].id,
        TagId: tagsRow[0].id
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        PostId: postsRow[0].id,
        TagId: tagsRow[3].id
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        PostId: postsRow[0].id,
        TagId: tagsRow[4].id
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        PostId: postsRow[1].id,
        TagId: tagsRow[2].id
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        PostId: postsRow[1].id,
        TagId: tagsRow[3].id
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        PostId: postsRow[1].id,
        TagId: tagsRow[5].id
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        PostId: postsRow[2].id,
        TagId: tagsRow[0].id
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        PostId: postsRow[2].id,
        TagId: tagsRow[1].id
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        PostId: postsRow[2].id,
        TagId: tagsRow[5].id
      },
    ])
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('posts_tags', null, {})
  }
};
