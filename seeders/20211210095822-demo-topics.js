'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    const categories = await queryInterface.sequelize.query(
      `SELECT id FROM Categories`
    );
    const categoriesRow = categories[0]

    await queryInterface.bulkInsert('Topics', 
    [{
      name: 'COVID-19',
      category: categoriesRow[0].id,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: "Terre Plate",
      category: categoriesRow[1].id,
      createdAt: new Date(),
      updatedAt: new Date()
    }]
    )
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Topics', null, {})
  }
};
