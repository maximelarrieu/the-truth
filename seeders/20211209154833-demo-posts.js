'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    const users = await queryInterface.sequelize.query(
      `SELECT id FROM Users`
    );
    const usersRow = users[0];
    
    const topics = await queryInterface.sequelize.query(
      `SELECT id FROM Topics`
    )
    const topicsRow = topics[0];

    await queryInterface.bulkInsert('Posts', [
      {
        title: 'Le pass nazitaire.',
        content: `Bonsoir à tous.
        J'ai envie aujourd'hui de vous partager ma haine envers ce gouvernement et la mise en place de ce pass sanitaire qui est complètement liberticide. ON ACHETE NOTRE LIBERTE!
        Depuis quand ne nous sommes pas libre d'aller au restaurant, en boîte de nuit ou autre ? Sérieux, manifestez-vous tout cela n'est que manipulation, ne soyez pas des moutons!!!`,
        author: usersRow[0].id,
        status: 1,
        topic: topicsRow[0].id,
        createdAt: new Date(),
        updatedAt: new Date(),
        closedAt: new Date()
      },
      {
        title: 'Les preuves que la terre est plate.',
        content: `Salut tout le monde! Je voulais vous partager aujourd'hui mon expérience.
        Lors d'une balade champêtre avec mes petits enfants, nous avons gravis la dune du Pila.
        Et là... C'est la révélation !!! Aucun courbe à l'horizon, la mer était si plate que je suis sûr que l'on pourrait en voir la fin!
        C'est évident que le gourvernement nous cache des choses que nous ne sommes pas censé savoir !! Voyez par vous-mêmes!`,
        author: usersRow[0].id,
        status: 1,
        topic: topicsRow[1].id,
        createdAt: new Date(),
        updatedAt: new Date(),
        closedAt: new Date()
      },
      {
        title: 'Pfizer vous injecte la 5g!',
        content: `Bonjour,
        Aujourd'hui après la 32ème dose, je rentre chez moi et là... très mal au crâne. Impossible d'ouvrir les yeux tellement la douleur est immense...
        J'essaie de chercher des solutions sur internet grâce à mon téléphone portable et c'est la stupéfaction !
        Je capte la 5G sur mon téléphone qui n'est même pas éligible (Samsung A50) !! Bizarrement, à la sortie de mon dernier vaccin, il y a des choses pas net là-dedans!!!`,
        author: usersRow[1].id,
        status: 1,
        topic: topicsRow[0].id,
        createdAt: new Date(),
        updatedAt: new Date(),
        closedAt: new Date()
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    return queryInterface.bulkDelete('Posts', null, {});
  }
};
