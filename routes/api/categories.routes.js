const express = require('express');
const cors = require('cors')
const { authJwt } = require("../../middleware");

const router = express.Router();
const controller = require('../../controllers/categories.controller')

const corsOptions = {
	origin: 'http://localhost:3000',
	optionsSuccessStatus: 200
}

/**
   * @swagger
   * /api/categories:
   *   get:
   *     summary: List of all categories
   *     security:              
   *      - bearerAuth: [] 
   *     description: Returns all categories
   *     tags:
   *      - Categories
   *     produces:
   *      - application/json
   *     responses:
   *       200:
   *         description: all categories
   */
router.get('/', cors(corsOptions), [authJwt.verifyToken], controller.findAll);

/**
 * @swagger
 * /api/categories/{id}:
 *   get:
 *     summary: Get a categories by id
 *     security:              
 *      - bearerAuth: [] 
 *     tags:
 *      - Categories
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The category's id
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: categories info
*/
router.get('/:id', cors(corsOptions), [authJwt.verifyToken], controller.findOne);

/**
 * @swagger
 * /api/categories:
 *   post:
 *     summary: Create a category.
 *     description: route used to create a category
 *     tags:
 *      - Categories
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 description: The name of your category.
 *                 example: Flat earth
 * 
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: Category infos
*/
router.post('/', cors(corsOptions), [authJwt.verifyToken, authJwt.isAdmin], controller.create);

/**
 * @swagger
 * /api/categories/{id}:
 *   put:
 *     summary: Modify a category
 *     tags:
 *      - Categories
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The category's id
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 description: The new name of your category.
 *                 example: Donut earth
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: Update category
*/
router.put('/:id', cors(corsOptions), [authJwt.verifyToken, authJwt.isAdmin], controller.update);

/**
 * @swagger
 * /api/categories/{id}:
 *   delete:
 *     summary: Delete a category by id
 *     tags:
 *      - Categories
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The category's id
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: Delete category
*/
router.delete('/:id', cors(corsOptions), [authJwt.verifyToken, authJwt.isAdmin], controller.delete);

/**
 * @swagger
 * /api/categories/{id}/follow:
 *   post:
 *     summary: Follow a category to have proposed topics
 *     tags:
 *      - Categories
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The category's id
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: Follow a category
*/
router.post('/:id/follow', cors(corsOptions), [authJwt.verifyToken], controller.addUser)

/**
 * @swagger
 * /api/categories/{id}/unfollow:
 *   delete:
 *     summary: Unfollow a category
 *     tags:
 *      - Categories
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The category's id
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: Unfollow a category
*/
router.delete('/:id/unfollow', cors(corsOptions), [authJwt.verifyToken], controller.removeUser)

module.exports = router;
