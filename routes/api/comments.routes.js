const express = require('express');
const { authJwt } = require("../../middleware");

const router = express.Router();
const controller = require('../../controllers/comment.controller');

/**
   * @swagger
   * /api/comments:
   *   get:
   *     summary: List of all comments
   *     security:              
   *      - bearerAuth: [] 
   *     description: Returns all comments
   *     tags:
   *      - Comments
   *     parameters:
 *       - in: query
 *         name: page
 *         schema:
 *           type: string
 *         required: false
 *         description: The index of the page
 *       - in: query
 *         name: size
 *         schema:
 *           type: string
 *         required: false
 *         description: The size of the page
 *       - in: query
 *         name: post
 *         schema:
 *           type: string
 *         required: true
 *         description: The id of the post
   *     produces:
   *      - application/json
   *     responses:
   *       200:
   *         description: all comments
   */
router.get('/', [authJwt.verifyToken], controller.findAllByPost);

/**
 * @swagger
 * /api/comments/{id}:
 *   get:
 *     summary: Get a comment by id
 *     tags:
 *      - Comments
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The comment's id
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: comments info
*/
router.get('/:id', [authJwt.verifyToken], controller.findOne);

// router.get('/')

/**
 * @swagger
 * /api/comments:
 *   post:
 *     summary: Create a comment.
 *     description: route used to create a comment
 *     tags:
 *      - Comments
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               author:
 *                 type: int
 *                 description: The id of the author.
 *                 example: 1
 *               content:
 *                 type: text
 *                 description: The content of the comment.
 *                 example: "Aliens claim that the earth is round"
 *               post:
 *                 type: int
 *                 description: The id of the post this comment belongs to.
 *                 example: 1
 * 
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: comments infos
*/
router.post('/', [authJwt.verifyToken], controller.create);

/**
 * @swagger
 * /api/comments/{id}:
 *   put:
 *     summary: Modify a comment
 *     tags:
 *      - Comments
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The comment's id
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               author:
 *                 type: int
 *                 description: The id of the author.
 *                 example: 1
 *               content:
 *                 type: text
 *                 description: The content of the comment.
 *                 example: "Aliens claim that the earth has a donut shape"
 *               post:
 *                 type: int
 *                 description: The id of the post this comment belongs to.
 *                 example: 1
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: Update category
*/
router.put('/:id', [authJwt.verifyToken, authJwt.isAuthor], controller.update);

/**
 * @swagger
 * /api/comments/{id}:
 *   delete:
 *     summary: Delete a comment by id
 *     tags:
 *      - Comments
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The comment's id
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: Delete comment
*/
router.delete('/:id', [authJwt.verifyToken, authJwt.isAdminOrModerator, authJwt.isAuthor], controller.delete);

module.exports = router;
