const express = require('express');
const cors = require('cors')
const { authJwt } = require("../../middleware");

const router = express.Router();
const controller = require('../../controllers/topic.controller')

const corsOptions = {
	origin: 'http://localhost:3000',
	optionsSuccessStatus: 200
}

/**
   * @swagger
   * /api/topics:
   *   get:
   *     summary: List of all topics
   *     security:              
   *      - bearerAuth: [] 
   *     description: Returns all topics
   *     tags:
   *      - Topics
   *     produces:
   *      - application/json
   *     responses:
   *       200:
   *         description: all topics
   */
router.get('/', cors(corsOptions), [authJwt.verifyToken], controller.findAll);

/**
 * @swagger
 * /api/topics/{id}:
 *   get:
 *     summary: Get a topic by id
 *     tags:
 *      - Topics
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The topic's id
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: Topics info
*/
router.get('/:id', cors(corsOptions), [authJwt.verifyToken], controller.findOne);

/**
 * @swagger
 * /api/topics:
 *   post:
 *     summary: Create a topic.
 *     description: route used to create a topic
 *     tags:
 *      - Topics
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 description: The name of your topic.
 *                 example: UFOs in trouble !
 *               category:
 *                 type: int
 *                 description: The Id of the category the topic belongs to.
 *                 example: 2
 * 
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: Topic info
*/
router.post('/', cors(corsOptions), [authJwt.verifyToken, authJwt.isAdmin], controller.create);

/**
 * @swagger
 * /api/topics/{id}:
 *   put:
 *     summary: Modify a topic
 *     tags:
 *      - Topics
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: int
 *         required: true
 *         description: The topic's id
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 description: The new name of your topic.
 *                 example: UFOs in double trouble !
 *               category:
 *                 type: int
 *                 description: The Id of the category the topic now belongs to.
 *                 example: 2
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: Update topic
*/
router.put('/:id', cors(corsOptions), [authJwt.verifyToken, authJwt.isAdmin], controller.update);

/**
 * @swagger
 * /api/topics/{id}:
 *   delete:
 *     summary: Delete a topic by id
 *     tags:
 *      - Topics
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: int
 *         required: true
 *         description: The topic's id
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: Delete topic
*/
router.delete('/:id', cors(corsOptions), [authJwt.verifyToken, authJwt.isAdmin], controller.delete);

/**
 * @swagger
 * /api/topics/{id}/follow:
 *   post:
 *     summary: Follow a topic to have proposed posts
 *     tags:
 *      - Topics
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The topic's id
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: Follow a topic
*/
router.post('/:id/follow', cors(corsOptions), [authJwt.verifyToken], controller.addUser)

/**
 * @swagger
 * /api/topics/{id}/unfollow:
 *   delete:
 *     summary: Unfollow a topic
 *     tags:
 *      - Topics
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The topic's id
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: Unfollow a topic
*/
router.delete('/:id/unfollow', cors(corsOptions), [authJwt.verifyToken], controller.removeUser)

module.exports = router;
