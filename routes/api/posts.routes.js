const express = require('express');
const cors = require('cors')
const { authJwt } = require("../../middleware");

const router = express.Router();
const controller = require('../../controllers/post.controller')

const corsOptions = {
	origin: 'http://localhost:3000',
	optionsSuccessStatus: 200
}

/**
   * @swagger
   * /api/posts:
   *   get:
   *     summary: List of all posts
   *     security:              
   *      - bearerAuth: [] 
   *     description: Returns all posts
   *     tags:
   *      - Posts
   *     produces:
   *      - application/json
   *     responses:
   *       200:
   *         description: all posts
   */
router.get('/', cors(corsOptions), [authJwt.verifyToken], controller.findAll);

/**
 * @swagger
 * /api/posts/topic/{topic}:
 *   get:
 *     summary: Get posts by topic
 *     tags:
 *      - Posts
 *     parameters:
 *       - in: path
 *         name: topic
 *         schema:
 *           type: string
 *         required: true
 *         description: The topic's id
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: Post infos by topic
*/
router.get('/topic/:topic', cors(corsOptions), [authJwt.verifyToken], controller.findAllByTopic);

/**
 * @swagger
 * /api/posts/{id}:
 *   get:
 *     summary: Get a post by id
 *     tags:
 *      - Posts
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The post's id
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: Post info
*/
router.get('/:id', cors(corsOptions), [authJwt.verifyToken], controller.findOne);

/**
 * @swagger
 * /api/post:
 *   post:
 *     summary: Create a post.
 *     description: route used to create a post
 *     tags:
 *      - Posts
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               title:
 *                 type: string
 *                 description: The title of your post.
 *                 example: UFOs in trouble !
 * 				 content:
 * 				   type: string
 * 				   description: The content of post.
 *               topic:
 *                 type: int
 *                 description: The Id of the topic the topic belongs to.
 *                 example: 2
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: Post info
*/
router.post('/', cors(corsOptions), [authJwt.verifyToken], controller.create);

/**
 * @swagger
 * /api/posts/{id}:
 *   put:
 *     summary: Modify a post
 *     tags:
 *      - Posts
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: int
 *         required: true
 *         description: The post's id
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               title:
 *                 type: string
 *                 description: The new title of your post.
 *                 example: UFOs in double trouble !
 * 				 content:
 * 				   type: string
 * 				   description: The new content of the post.
 *               topic:
 *                 type: int
 *                 description: The Id of the category the topic now belongs to.
 *                 example: 2
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: Update topic
*/
router.put('/:id', cors(corsOptions), [authJwt.verifyToken, authJwt.isAuthor], controller.update);

/**
 * @swagger
 * /api/posts/{id}/status:
 *   put:
 *     summary: Modify a post's status
 *     tags:
 *      - Posts
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: int
 *         required: true
 *         description: The post's id
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               status:
 *                 type: int
 *                 description: The Id of the status the topic now belongs to.
 *                 example: 2
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: Update topic
*/
router.put('/:id/status', cors(corsOptions), [authJwt.verifyToken, authJwt.isAdminOrModerator, authJwt.isAuthor], controller.updateStatus);

/**
 * @swagger
 * /api/posts/{id}:
 *   delete:
 *     summary: Delete a post by id
 *     tags:
 *      - Posts
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: int
 *         required: true
 *         description: The post's id
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: Delete post
*/
router.delete('/:id', cors(corsOptions), [authJwt.verifyToken, authJwt.isAdminOrModerator, authJwt.isAuthor], controller.delete);

/**
 * @swagger
 * /api/posts/{id}/add/{tag_id}/tag:
 *   post:
 *     summary: Follow a tag to have proposed posts
 *     tags:
 *      - Posts
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The tag's id
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: Follow a tag
*/
router.post('/:id/add/:tag_id/tag', cors(corsOptions), [authJwt.verifyToken, authJwt.isAdminOrModerator, authJwt.isAuthor], controller.addTag)

/**
 * @swagger
 * /api/posts/{id}/remove/{tag_id}/tag:
 *   delete:
 *     summary: Unfollow a tag to have proposed posts
 *     tags:
 *      - Posts
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The tag's id
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: Unfollow a tag
*/
router.delete('/:id/remove/:tag/tag', cors(corsOptions), [authJwt.verifyToken, authJwt.isAuthor, authJwt.isAdminOrModerator], controller.removeTag)

/**
 * @swagger
 * /api/posts/{id}/follow:
 *   post:
 *     summary: Follow a post to have notification about the post
 *     tags:
 *      - Posts
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The post's id
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: Follow a post
*/
router.post('/:id/follow', cors(corsOptions), [authJwt.verifyToken], controller.addUser)

/**
 * @swagger
 * /api/posts/{id}/unfollow:
 *   delete:
 *     summary: Unollow a post
 *     tags:
 *      - Posts
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The post's id
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: Unfollow a post
*/
router.delete('/:id/unfollow', cors(corsOptions), [authJwt.verifyToken], controller.removeUser)

module.exports = router;