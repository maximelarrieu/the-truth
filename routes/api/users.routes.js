const express = require('express');
const cors = require('cors');
const { verifyRegister, authJwt } = require("../../middleware");
const controller = require('../../controllers/user.controller')


const router = express.Router();

const corsOptions = {
	origin: 'http://localhost:3000',
	optionsSuccessStatus: 200
}
/**
 * @swagger
 * /api/users/register:
 *   post:
 *     summary: Create an acount.
 *     description: route used for acount creation
 *     tags:
 *      - Auth
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               username:
 *                 type: string
 *                 description: The username that will be used for login.
 *                 example: Didier69
 *               email:
 *                 type: string
 *                 description: The email will be used for notifications.
 *                 example: Didier69@gmail.com
 *               password:
 *                 type: string
 *                 description: The password that will be used for login.
 *                 example: 1234joo
 * 
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: user info
*/
router.post('/register', cors(corsOptions), [verifyRegister.checkDuplicateUsernameOrEmail], controller.signup)

/**
 * @swagger
 * /api/users/login:
 *   post:
 *     summary: signin with an acount.
 *     description: route used for signin
 *     tags:
 *      - Auth
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               username:
 *                 type: string
 *                 description: The username that will be used for login.
 *                 example: maximelarrieu
 *               password:
 *                 type: string
 *                 description: The password that will be used for login.
 *                 example: password
 * 
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: user info
*/
router.post('/login', cors(corsOptions), controller.signin)
  /**
   * @swagger
   * /api/users:
   *   get:
   *     summary: list of all users
   *     security:              
   *      - bearerAuth: [] 
   *     description: Returns all users
   *     tags:
   *      - Users
   *     produces:
   *      - application/json
   *     responses:
   *       200:
   *         description: all users
   */
router.get('/', cors(corsOptions), [authJwt.verifyToken], controller.findAll)

/**
 * @swagger
 * /api/users/{id}:
 *   get:
 *     summary: Get a user by id
 *     tags:
 *      - Users
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The user's id
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: user info
*/
router.get('/:id', cors(corsOptions), [authJwt.verifyToken], controller.findOne)

/**
 * @swagger
 * /api/users/{id}/profile/image:
 *   get:
 *     summary: Get a user avatar
 *     tags:
 *      - Users
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The user's avatar
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: user avatar
*/
router.put('/:id/profile/image', (req, res) => {
	res.send('Modify users image');
});

/**
 * @swagger
 * /api/users/{id}:
 *   put:
 *     summary: modify a user by his id
 *     tags:
 *      - Users
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The user's id
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               username:
 *                 type: string
 *                 description: new username.
 *                 example: Didier69
 *               email:
 *                 type: string
 *                 description: The new email.
 *                 example: Didier69@gmail.com
 *               password:
 *                 type: string
 *                 description: The new password.
 *                 example: 1234joo
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: user info
*/
router.put('/:id', cors(corsOptions), [authJwt.verifyToken, authJwt.isAdmin], controller.update)

/**
 * @swagger
 * /api/users/{id}/role:
 *   put:
 *     summary: modify a user's role by his id
 *     tags:
 *      - Users
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The user's id
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               role:
 *                 type: integer
 *                 description: The new role (1 visitor, 2 moderator, 3 administrator).
 *                 example: 1
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: user info
*/
router.put('/:id/role', cors(corsOptions), [authJwt.verifyToken, authJwt.isAdmin], controller.updateRole)

/**
 * @swagger
 * /api/users/{id}:
 *   delete:
 *     summary: delete a user by id
 *     tags:
 *      - Users
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The user's id
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: user info
*/
router.delete('/:id', cors(corsOptions), [authJwt.verifyToken, authJwt.isAdmin], controller.delete)

module.exports = router;
