const express = require('express');
const cors = require('cors')
const { authJwt } = require("../../middleware");

const router = express.Router();
const controller = require('../../controllers/role.controller')

const corsOptions = {
	origin: 'http://localhost:3000',
	optionsSuccessStatus: 200
}

/**
   * @swagger
   * /api/roles:
   *   get:
   *     summary: List of all roles
   *     security:              
   *      - bearerAuth: [] 
   *     description: Returns all roles
   *     tags:
   *      - Roles
   *     produces:
   *      - application/json
   *     responses:
   *       200:
   *         description: all roles
   */
router.get('/', cors(corsOptions), [authJwt.verifyToken], controller.findAll);

/**
 * @swagger
 * /api/roles/{id}:
 *   get:
 *     summary: Get a role by id
 *     tags:
 *      - Roles
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The role's id
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: role info
*/
router.get('/:id', cors(corsOptions), [authJwt.verifyToken], controller.findOne);

/**
 * @swagger
 * /api/roles:
 *   post:
 *     summary: Create a role.
 *     description: route used to create a role
 *     tags:
 *      - Roles
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 description: The name of your role.
 *                 example: bg
 * 
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: Role infos
*/
router.post('/', cors(corsOptions), [authJwt.verifyToken, authJwt.isAdmin], controller.create);

/**
 * @swagger
 * /api/roles/{id}:
 *   put:
 *     summary: Modify a tag
 *     tags:
 *      - Roles
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The role's id
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 description: The new name of your role.
 *                 example: mega bg
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: Update role
*/
router.put('/:id', cors(corsOptions), [authJwt.verifyToken, authJwt.isAdmin], controller.update);

/**
 * @swagger
 * /api/roles/{id}:
 *   delete:
 *     summary: Delete a role by id
 *     tags:
 *      - Roles
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The role's id
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: Delete role
*/
router.delete('/:id', cors(corsOptions), [authJwt.verifyToken, authJwt.isAdmin], controller.delete);

module.exports = router;
