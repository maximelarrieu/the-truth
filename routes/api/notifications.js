const express = require('express');

const router = express.Router();

router.get('/', (req, res) => {
	res.send('Get notification');
});

router.post('/', (req, res) => {
	res.send('Send notification');
});

module.exports = router;
