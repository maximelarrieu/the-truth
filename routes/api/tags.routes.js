const express = require('express');
const cors = require('cors')
const { authJwt } = require("../../middleware");

const router = express.Router();
const controller = require('../../controllers/tag.controller')

const corsOptions = {
	origin: 'http://localhost:3000',
	optionsSuccessStatus: 200
}

/**
   * @swagger
   * /api/tags:
   *   get:
   *     summary: List of all tags
   *     security:              
   *      - bearerAuth: [] 
   *     description: Returns all tags
   *     tags:
   *      - Tags
   *     produces:
   *      - application/json
   *     responses:
   *       200:
   *         description: all tags
   */
router.get('/', cors(corsOptions), [authJwt.verifyToken], controller.findAll);

/**
 * @swagger
 * /api/tags/{id}:
 *   get:
 *     summary: Get a tag by id
 *     tags:
 *      - Tags
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The tag's id
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: Tag info
*/
router.get('/:id', cors(corsOptions), [authJwt.verifyToken], controller.findOne);

/**
 * @swagger
 * /api/tags:
 *   post:
 *     summary: Create a tag.
 *     description: route used to create a tag
 *     tags:
 *      - Tags
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 description: The name of your tag.
 *                 example: nsfw
 * 
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: Tag infos
*/
router.post('/', cors(corsOptions), [authJwt.verifyToken, authJwt.isAdmin], controller.create);

/**
 * @swagger
 * /api/tags/{id}:
 *   put:
 *     summary: Modify a tag
 *     tags:
 *      - Tags
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The tag's id
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 description: The new name of your tag.
 *                 example: sfw
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: Update tag
*/
router.put('/:id', cors(corsOptions), [authJwt.verifyToken, authJwt.isAdmin], controller.update);

/**
 * @swagger
 * /api/tags/{id}:
 *   delete:
 *     summary: Delete a tag by id
 *     tags:
 *      - Tags
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The tag's id
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: Delete tag
*/
router.delete('/:id', cors(corsOptions), [authJwt.verifyToken, authJwt.isAdmin], controller.delete);

/**
 * @swagger
 * /api/tags/{id}/follow:
 *   post:
 *     summary: Follow a tag to have proposed posts
 *     tags:
 *      - Tags
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The tag's id
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: Follow a tag
*/
router.post('/:id/follow', cors(corsOptions), [authJwt.verifyToken], controller.addUser)

/**
 * @swagger
 * /api/tags/{id}/unfollow:
 *   delete:
 *     summary: Unfollow a tag
 *     tags:
 *      - Tags
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The tag's id
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: Unfollow a tag
*/
router.delete('/:id/unfollow', cors(corsOptions), [authJwt.verifyToken], controller.removeUser)

module.exports = router;
