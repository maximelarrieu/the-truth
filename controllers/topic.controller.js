const db = require('../models');
const Topic = db.Topic;
const Category = db.Category;
const User = db.User;

exports.findAll = async (req, res) => {
    try {
        await Topic.findAll()
        .then(topics => {
            return res.status(200).send({
                status: "success",
                message: "All topics have been retrieved.",
                topics: topics
            })
        })
        .catch(err => {
            return res.status(500).send({ status: "error", message: err.message })
        })
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    }
}

exports.findOne = async (req, res) => {
    try {
        const id = req.params.id;

        await Topic.findByPk(id)
        .then(async topic => {
            if(!topic) {
                return res.status(404).send({
                    status: "error",
                    message: "The topic was not found.",
                    topic: null
                })
            } else {
                let category = await Category.findByPk(topic.category)
                return res.status(200).send({
                    status: "success",
                    message: "The topic was found.",
                    topic : {
                        name: topic.name,
                        category: {
                            name: category.name,
                            createdAt: category.createdAt,
                            updatedAt: category.updatedAt
                        },
                        createdAt: topic.createdAt,
                        updatedAt: topic.updatedAt
                    }
                })
            }
        })
        .catch(error => {
            res.status(500).send({
                status: "error",
                message: "Error retriving Topic with id=" + id + ' | ' + error.message
            })
        })
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    }
}

exports.create = async (req, res) => {
    try {
        if(!req.body.name) {
            return res.status(400).send({
                status: "error",
                message: "Name can not be empty."
            })
        }

        const topic = {
            name: req.body.name,
            category: req.body.category,
            createdAt: new Date(),
            updatedAt: new Date()
        }

        let category = await Category.findByPk(req.body.category)

        await Topic.create(topic)
        .then(topic => {
            return res.status(200).send({
                status: "success",
                message: "The topic has been created.",
                topic : {
                    name: topic.name,
                    category: {
                        name: category.name,
                        createdAt: category.createdAt,
                        updatedAt: category.updatedAt
                    },
                    id: topic.id,
                    createdAt: topic.createdAt,
                    updatedAt: topic.updatedAt
                }
            })
        })
        .catch(error => {
            return res.status(500).send({
                status: "error",
                message: "The topic has not been created. | " + error.message,
                comment: null
            })
        })
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    }
}

exports.update = async (req, res) => {
    try {
        const id = req.params.id;

        Topic.update(req.body, {
            where: {id: id}
        })
        .then(async () => {
            await Topic.findByPk(id)
            .then(updated => {
                return res.status(200).send({
                    status: 'success',
                    message: "The topic was updated.",
                    topic: updated
                })
            })
        })
        .catch(error =>{
            return res.status(500).send({
                status: 'error',
                message: "The topic has not been updated. | " + error.message,
                topic: null
            })
        })
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    }
}

exports.delete = async (req, res) => {
    try {
        const id = req.params.id;

        if(!id) {
            return res.status(404).send({
                status: 'error',
                message: `The topic ${id} was not found.`
            })
        }
        Topic.destroy({
            where: {id: id}
        })
        .then(async () => {
            return res.status(200).send({
                status: 'success',
                message: 'The topic was deleted.'
            })
        })
        .catch(error => {
            return res.status(500).send({
                status: "error",
                message: "The topic was not deleted. | " + error,
                post: null
            })
        }) 
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    }
}

exports.addUser = async (req, res) => {
    try {
        const id = req.params.id;

        await Topic.findByPk(id)
            .then(async topic => {
                if(!topic) {
                    return res.status(404).send({
                        status: "error", 
                        message: `The topic was not found.`,
                        topic: null
                    })
                }
                await User.findByPk(req.userId)
                    .then(user => {
                        if(!user) {
                            return res.status(404).send({
                                status: "error", 
                                message: `The user was not found.`,
                                user: null
                            })
                        }

                        user.addTopic(topic);
                        
                        return res.status(200).send({
                            status: "success",
                            message: "You followed the category.",
                            topic: topic
                        })                     
                    })
                    .catch(err => {
                        return res.status(500).send({ status: "error", message: err.message})
                    })
            })
            .catch(err => {
                return res.status(500).send({ status: "error", message: err.message})
            })
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    } 
}

exports.removeUser = async (req, res) => {
    try {
        const id = req.params.id;

        await Topic.findByPk(id)
            .then(async topic => {
                if(!topic) {
                    return res.status(404).send({
                        status: "error", 
                        message: `The topic was not found.`,
                        category: null
                    })
                }
                await User.findByPk(req.userId)
                    .then(user => {
                        if(!user) {
                            return res.status(404).send({
                                status: "error", 
                                message: `The user was not found.`,
                                user: null
                            })
                        }

                        user.removeTopic(topic);
                        
                        return res.status(200).send({
                            status: "success",
                            message: "You unfollowed the topic.",
                            topic: topic
                        })                     
                    })
                    .catch(err => {
                        return res.status(500).send({ status: "error", message: err.message})
                    })
            })
            .catch(err => {
                return res.status(500).send({ status: "error", message: err.message})
            })
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    } 
}