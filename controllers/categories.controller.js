const db = require("../models");
const User = db.User;
const Post = db.Post;
const Comment = db.Comment;
const Role = db.Role;
const Category = db.Category;
const Topic = db.Topic;

exports.findAll = async (req, res) => {
    try {
        await Category.findAll()
        .then(categories => {
            return res.status(200).send({
                status: "success",
                message: "All categories have been retrieved.",
                categories: categories
            })
        })
        .catch(err => {
            return res.status(500).send({ status: "error", message: err.message })
        })
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    }
}

exports.findOne = async (req, res) => {
    try {
        const id = req.params.id;

        await Category.findByPk(id)
        .then(async category => {
            if(!category) {
                return res.status(404).send({
                    status: "error",
                    message: "The comment was not found.",
                    category: null
                })
            } else {
                return res.status(200).send({
                    status: "success",
                    message: "The comment was found.",
                    category: category
                })
            }
        })
        .catch(error => {
            res.status(500).send({
                status: "error",
                message: "Error retriving Category with id=" + id + ' | ' + error.message
            })
        })
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    }
}

exports.create = async (req, res) => {
    try {
        if(!req.body.name) {
            return res.status(400).send({
                status: "error",
                message: "Name can not be empty."
            })
        }

        const category = {
            name: req.body.name,
            createdAt: new Date(),
            updatedAt: new Date()
        }

        await Category.create(category)
        .then(category => {
            return res.status(200).send({
                status: "success",
                message: "The category has been created.",
                category: category
            })
        })
        .catch(error => {
            return res.status(500).send({
                status: "success",
                message: "The category has not been created. | " + error.message,
                comment: null
            })
        })
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    }
}

exports.update = async (req, res) => {
    try {
        const id = req.params.id;

        Category.update(req.body, {
            where: {id: id}
        })
        .then(async () => {
            await Category.findByPk(id)
            .then(updated => {
                return res.status(200).send({
                    status: "success",
                    message: "The category was updated.",
                    post: updated
                })
            })
        })
        .catch(error => {
            return res.status(500).send({
                status: "error",
                message: "The category has not been updated. | " + error,
                post: null
            })
        })
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    }
}

exports.delete = async (req, res) => {
    try {
        const id = req.params.id;
        
        if(!id) {
            return res.status(404).send({
                status: "error",
                message: `The category ${id} was not found.`
            })
        }

        Category.destroy({
            where: { id: id }
        })
        .then(async () => {
            return res.status(200).send({
                status: "success",
                message: "The category was deleted."
            })
        })
        .catch(error => {
            return res.status(500).send({
                status: "error",
                message: "The category was not deleted. | " + error,
                post: null
            })
        }) 
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    }
}

exports.addUser = async (req, res) => {
    try {
        const id = req.params.id;

        await Category.findByPk(id)
            .then(async category => {
                if(!category) {
                    return res.status(404).send({
                        status: "error", 
                        message: `The category was not found.`,
                        category: null
                    })
                }
                await User.findByPk(req.userId)
                    .then(user => {
                        if(!user) {
                            return res.status(404).send({
                                status: "error", 
                                message: `The user was not found.`,
                                user: null
                            })
                        }

                        user.addCategory(category);
                        
                        return res.status(200).send({
                            status: "success",
                            message: "You followed the category.",
                            category: category
                        })                     
                    })
                    .catch(err => {
                        return res.status(500).send({ status: "error", message: err.message})
                    })
            })
            .catch(err => {
                return res.status(500).send({ status: "error", message: err.message})
            })
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    } 
}

exports.removeUser = async (req, res) => {
    try {
        const id = req.params.id;

        await Category.findByPk(id)
            .then(async category => {
                if(!category) {
                    return res.status(404).send({
                        status: "error", 
                        message: `The category was not found.`,
                        category: null
                    })
                }
                await User.findByPk(req.userId)
                    .then(user => {
                        if(!user) {
                            return res.status(404).send({
                                status: "error", 
                                message: `The user was not found.`,
                                user: null
                            })
                        }

                        user.removeCategory(category);
                        
                        return res.status(200).send({
                            status: "success",
                            message: "You unfollowed the category.",
                            category: category
                        })                     
                    })
                    .catch(err => {
                        return res.status(500).send({ status: "error", message: err.message})
                    })
            })
            .catch(err => {
                return res.status(500).send({ status: "error", message: err.message})
            })
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    } 
}