exports.getPagination = (page, size) => {
    const limit = size ? +size : 2;
    const offset = page ? page * limit : 0;
    return { limit, offset }
}

exports.getPagingData = (data, page, limit) => {
    const { count: totalPosts, rows: posts } = data;
    const currentPage = (page + 1) ? (+page + 1) : 0;
    const totalPages = Math.ceil(totalPosts / limit);
  
    return { status: "success", message: "All posts have been retrieved.", totalPosts, posts, totalPages, currentPage };
};