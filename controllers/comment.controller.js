const MailService = require('../services/mail_service');
const mailService = new MailService;

const db = require("../models");
const User = db.User;
const Post = db.Post;
const Comment = db.Comment;
const Role = db.Role;
const Category = db.Category;
const Topic = db.Topic;

const paginationController = require('./pagination.controller');

exports.findAll = async (req, res) => {
    try {
        await Comment.findAll()
        .then(comments => {
            return res.status(200).send({
                status: "success",
                message: "All comments have been retrieved.",
                comments: comments
            })
        })
        .catch(err => {
            return res.status(500).send({ status: "error", message: err.message })
        })
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    }
}

exports.findAllByPost = async (req, res) => {
    try {
        const { post, page, size } = req.query;
        const { limit, offset } = paginationController.getPagination(page, size);
        await Comment.findAndCountAll({
            where: {post: post},
            limit, offset
        })
        .then(comments => {
            return res.status(200).send(
                paginationController.getPagingData(comments, page, limit)
            )
        })
        .catch(err => {
            return res.status(500).send({ status: "error", message: err.message})
        })

    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    }
}

exports.findOne = async (req, res) => {
    try {
        const id = req.params.id;
        console.log(id)

        await Comment.findByPk(id)
        .then(async comment => {
            if(!comment) {
                return res.status(404).send({
                    status: "error",
                    message: "The comment was not found.",
                    comment: null
                })
            } else {
                let commentAuthor = await User.findOne({
                    where: { id: comment.author }
                })
                let commentRole = await Role.findOne({
                    where: { id: commentAuthor.role }
                })
                let post = await Post.findOne({
                    where: {id: comment.post}
                })
                let postAuthor = await User.findOne({
                    where: { id: post.author }
                })
                let postRole = await Role.findOne({
                    where: { id: postAuthor.role }
                })
                let topic = await Topic.findOne({
                    where: { id: post.topic }
                })
                let category = await Category.findOne({
                    where: { id: topic.category }
                })

                return res.status(200).send({
                    status: "success",
                    message: "The comment was found.",
                    comment: {
                        author: {
                            username: commentAuthor.username,
                            email: commentAuthor.email,
                            password: commentAuthor.password,
                            role: commentRole,
                            createdAt: commentAuthor.createdAt,
                            updatedAt: commentAuthor.updatedAt
                        },
                        content: comment.content,
                        post: {
                            title: post.title,
                            content: post.content,
                            author: {
                                username: postAuthor.username,
                                email: postAuthor.email,
                                password: postAuthor.password,
                                role: postRole,
                                createdAt: postAuthor.createdAt,
                                updatedAt: postAuthor.updatedAt
                            },
                            topic: {
                                name: topic.name,
                                category: category,
                                createdAt: topic.createdAt,
                                updatedAt: topic.updatedAt
                            }
                        },
                        createdAt: comment.createdAt,
                        updatedAt: comment.updatedAt
                    }
                })
            }
        })
        .catch(error => {
            res.status(500).send({
                status: "error",
                message: "Error retriving Comment with id=" + id + ' | ' + error.message
            })
        })
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    }
}

exports.create = async (req, res) => {
    try {
        if(!req.body.content) {
            return res.status(400).send({
                status: "error",
                message: "Content can not be empty."
            })
        }

        const comment = {
            content: req.body.content,
            author: req.userId,
            post: 1,
            createdAt: new Date(),
            updatedAt: new Date()
        }

        let post = await Post.findOne({
            where: {id: comment.post}
        })
        let postAuthor = await User.findOne({
            where: { id: post.author }
        })        

        await Comment.create(comment)
        .then(async comment => {
            let commentAuthor = await User.findOne({
                where: {id: comment.author}
            })
            mailService.sendMail(
                postAuthor.email,
                `L'utilisateur ${commentAuthor.username} a commenté votre post ${post.title}.`,
                `Votre post a été commenté par ${commentAuthor.username} : ${comment.content}`);

            return res.status(200).send({
                status: "success",
                message: "The comment has been created.",
                comment: comment
            })
        })
        .catch(error => {
            return res.status(500).send({
                status: "error",
                message: "The comment has not been created. | " + error.message,
                comment: null
            })
        })
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    }
}

exports.update = async (req, res) => {
    try {
        const id = req.params.id;

        Comment.update(req.body, {
            where: {id: id}
        })
        .then(async () => {
            await Comment.findByPk(id)
            .then(updated => {
                return res.status(200).send({
                    status: "success",
                    message: "The comment was updated.",
                    comment: updated
                })
            })
        })
        .catch(error => {
            return res.status(500).send({
                status: "error",
                message: "The comment has not been updated. | " + error,
                comment: null
            })
        })
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    }
}

exports.delete = async (req, res) => {
    try {
        const id = req.params.id;
        
        if(!id) {
            return res.status(404).send({
                status: "error",
                message: `The comment ${id} was not found.`
            })
        }

        Comment.destroy({
            where: { id: id }
        })
        .then(async () => {
            return res.status(200).send({
                status: "success",
                message: "The comment was deleted."
            })
        })
        .catch(error => {
            return res.status(500).send({
                status: "error",
                message: "The comment was not deleted. | " + error,
                comment: null
            })
        }) 
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    }
}