const db = require("../models");
const Role = db.Role;

exports.findAll = async (req, res) => {
    try {
        await Role.findAll()
        .then(roles => {
            return res.status(200).send({
                status: 'success',
                message: "All roles have been retrieved.",
                roles: roles
            })
        })
        .catch(err => {
            return res.status(500).send({ status: "error", message: err.message})
        })
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    }
}

exports.findOne = async (req, res) => {
    try {
        const id = req.params.id;

        await Role.findByPk(id)
        .then(async role => {
            if(!role) {
                return res.status(404).send({
                    status: 'error',
                    message: "The role was not found.",
                    role: null
                })
            } else {
                return res.status(200).send({
                    status: 'success',
                    message: "The role was found.",
                    role: role
                })
            }
        })
        .catch(error => {
            res.status(500).send({
                status: "error",
                message: "Error retrieving Role with id=" + id + " | " + error
            });
        })
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    }
}

exports.create = async (req, res) => {
    try {
        if (!req.body.name) {
            return res.status(400).send({
                status: "error",
                message: "Content can not be empty."
            })
        }

        const role = {
            name: req.body.name,
            createdAt: new Date(),
            updatedAt: new Date(),
        }

        await Role.create(role)
        .then(role => {
            return res.status(200).send({
                status: "success",
                message: "The role has been created.",
                role: role
            })
        })
        .catch(error => {
            return res.status(500).send({
                status: "error",
                message: "The role has not been created. | " + error,
                post: null
            })
        })
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    }
}

exports.update = async (req, res) => {
    try {
        const id = req.params.id

        Role.update(req.body, {
            where: {id:id}
        })
        .then(async () => {
            await Role.findByPk(id)
            .then(updated => {
                return res.status(200).send({
                    status: "error",
                    message: "The role was updated.",
                    role: updated
                })
            })
        })
        .catch(error => {
            return res.status(500).send({
                status: "error",
                message: "The role has not been updated. | " + error,
                post: null
            })
        })
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    }
}

exports.delete = async (req, res) => {
    try {
        const id = req.params.id;
        
        if(!id) {
            return res.status(404).send({
                status: "error",
                message: `The role ${id} was not found.`
            })
        }

        Role.destroy({
            where: { id: id }
        })
        .then(async () => {
            return res.status(200).send({
                status: "success",
                message: "The role was deleted."
            })
        })
        .catch(error => {
            return res.status(500).send({
                status: "error",
                message: "The role was not deleted. | " + error,
                role: null
            })
        }) 
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    }
}
