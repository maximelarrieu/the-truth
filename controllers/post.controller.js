const { Op } = require("sequelize");

const db = require("../models");
const Post = db.Post;
const User = db.User;
const UserPost = db.UsersPosts;
const Topic = db.Topic;
const Role = db.Role;
const Category = db.Category;
const Tag = db.Tag;
const Comment = db.Comment;

const paginationController = require("./pagination.controller")
  
exports.findAll = async (req, res) => {
    try {
        const { page, size } = req.query;
        const { limit, offset } = paginationController.getPagination(page, size);

        await Post.findAndCountAll({limit, offset,
            include: [
                { model: Comment, include: [{ model: User }]}
            ]
        })
        .then(posts => {
            return res.status(200).send(
                paginationController.getPagingData(posts, page, limit)
            )
        })
        .catch(err => {
            return res.status(500).send({ status: "error", message: err.message})
        })
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    }
}

exports.findAllByTopic = async (req, res) => {
    try {
        const { page, size } = req.query;
        const { limit, offset } = paginationController.getPagination(page, size);

        let topic = req.params.topic

        await Post.findAndCountAll({
            where: { topic: topic },
            limit, offset
        })
        .then(posts => {
            return res.status(200).send(
                paginationController.getPagingData(posts, page, limit)
            )
        })
        .catch(err => {
            return res.status(500).send({ status: "error", message: err.message})
        })
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    }
}

exports.findOne = async (req, res) => {
    try {
        const id = req.params.id;

        await Post.findByPk(id, {
            include: [
                { model: Tag },
                { model: Comment, include: [ { model: User }] },
                { model: Topic, include: [ { model: Category }] },
            ],
            attributes: [
                "id",
                "title",
                "content",
                "status",
                "author",
                "createdAt",
                "updatedAt"
            ]
        })
        .then(async post => {
            if(!post) {
                return res.status(404).send({
                    status: "error",
                    message: "The post was not found.",
                    post: null
                })
            } else {
                let author = await User.findOne({
                    where: { id: post.author }
                })
                let role = await Role.findOne({
                    where: { id: author.role }
                })

                return res.status(200).send({
                    status: "success",
                    message: "The post was found.",
                    post: [post, {pauthor: {
                            username: author.username,
                            email: author.email,
                            password: author.password,
                            role: role,
                            createdAt: author.createdAt,
                            updateAt: author.updatedAt,
                    }}]
                })
            }
        })
        .catch(error => {
            res.status(500).send({
                status: "error",
                message: "Error retrieving Post with id=" + id + " | " + error
            });
        })

    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    }
}

exports.create = async (req, res) => {
    try {
        if (!req.body.title) {
            return res.status(400).send({
                status: "error",
                message: "Title can not be empty."
            });
        } else if(!req.body.content) {
            return res.status(400).send({
                status: "error",
                message: "Content can not be empty."
            });
        }

        const post = {
            title: req.body.title,
            content: req.body.content,
            author: req.userId,
            status: 0,
            topic: 1,
            createdAt: new Date(),
            updatedAt: new Date(),
        };

        await Post.create(post)
        .then(post => {
            return res.status(200).send({
                status: "success",
                message: "The post has been created.",
                post: post
            })
        })
        .catch(error => {
            return res.status(500).send({
                status: "error",
                message: "The post has not been created. | " + error,
                post: null
            })
        })
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    }
}

exports.update = async (req, res) => {
    try {
        const id = req.params.id;

        Post.update(req.body, {
            where: {id: id}
        })
        .then(async () => {
            await Post.findByPk(id)
            .then(updated => {
                return res.status(200).send({
                    status: "success",
                    message: "The post was updated.",
                    post: updated
                })
            })
        })
        .catch(error => {
            return res.status(500).send({
                status: "error",
                message: "The post has not been updated. | " + error,
                post: null
            })
        })
    } catch(error) {
        return res.status(500).send({ status: "erroooor", message: error.message })
    }
}

exports.updateStatus = async (req, res) => {
    try {
        const id = req.params.id;

        Post.update(req.body, {
            where: {id: id}
        })
        .then(async () => {
            await Post.findByPk(id)
            .then(async updated => {
                return res.status(200).send({
                    status: "success",
                    message: "The post's status was updated.",
                    post: updated
                })
            })
        })
        .catch(error => {
            return res.status(500).send({
                status: "error",
                message: "The post has not been updated. | " + error,
                post: null
            })
        })
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    }
}

exports.delete = async (req, res) => {
    try {
        const id = req.params.id;
        
        if(!id) {
            return res.status(404).send({
                status: "error",
                message: `The post ${id} was not found.`
            })
        }

        Post.destroy({
            where: { id: id }
        })
        .then(async () => {
            return res.status(200).send({
                status: "success",
                message: "The post was deleted."
            })
        })
        .catch(error => {
            return res.status(500).send({
                status: "error",
                message: "The post was not deleted. | " + error,
                post: null
            })
        }) 
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    }
}

exports.addTag = async (req, res) => {
    try {
        const id = req.params.id;

        await Tag.findByPk(id)
            .then(async tag => {
                if(!tag) {
                    return res.status(404).send({
                        status: "error", 
                        message: `The tag was not found.`,
                        tag: null
                    })
                }
                await Post.findByPk(req.userId)
                    .then(post => {
                        if(!post) {
                            return res.status(404).send({
                                status: "error", 
                                message: `The post was not found.`,
                                post: null
                            })
                        }

                        post.addTag(tag);
                        
                        return res.status(200).send({
                            status: "success",
                            message: `You add the tag ${tag.id} for your post.`,
                            tag: tag
                        })                     
                    })
                    .catch(err => {
                        return res.status(500).send({ status: "error", message: err.message})
                    })
            })
            .catch(err => {
                return res.status(500).send({ status: "error", message: err.message})
            })
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    } 
}

exports.removeTag = async (req, res) => {
    try {
        const id = req.params.id;

        await Tag.findByPk(id)
            .then(async tag => {
                if(!tag) {
                    return res.status(404).send({
                        status: "error", 
                        message: `The tag was not found.`,
                        tag: null
                    })
                }
                await Post.findByPk(req.userId)
                    .then(post => {
                        if(!post) {
                            return res.status(404).send({
                                status: "error", 
                                message: `The post was not found.`,
                                post: null
                            })
                        }

                        post.removeTag(tag);
                        
                        return res.status(200).send({
                            status: "success",
                            message: `You removed the tag ${tag.id} for your post.`,
                            tag: tag
                        })                     
                    })
                    .catch(err => {
                        return res.status(500).send({ status: "error", message: err.message})
                    })
            })
            .catch(err => {
                return res.status(500).send({ status: "error", message: err.message})
            })
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    } 
}

exports.addUser = async (req, res) => {
    try {
        const id = req.params.id;

        await Post.findByPk(id)
            .then(async post => {
                if(!post) {
                    return res.status(404).send({
                        status: "error", 
                        message: `The post was not found.`,
                        post: null
                    })
                }
                await User.findByPk(req.userId)
                    .then(user => {
                        console.log("USER", user)
                        if(!user) {
                            return res.status(404).send({
                                status: "error", 
                                message: `The user was not found.`,
                                user: null
                            })
                        }
                        const userPost = {
                            user: user.id,
                            post: post.id
                        }
                        UserPost.create(userPost)
                        .then(() => {
                            return res.status(200).send({
                                status: "success",
                                message: "You followed the post.",
                                post: post
                            })
                        })
                        .catch(err => {
                            return res.status(500).send({ status: "error", message: "NEIN " + err.message})
                        })
                    })
            })
            .catch(err => {
                return res.status(500).send({ status: "error", message: "NEINE " + err.message})
            })
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    } 
}

exports.removeUser = async (req, res) => {
    try {
        const user = req.userId;
        const post = req.params.id;

        UserPost.destroy({
            where: {
                [Op.and]: [
                    { user: user },
                    { post: post }
                ]
            }
        })
        .then(async () => {
            return res.status(200).send({
                status: 'success',
                message: 'You unfollowed the post.'
            })
        })
        .catch(error => {
            return res.status(500).send({
                status: "error",
                message: "Impossible to unfollowed the post. | " + error,
                post: null
            })
        }) 
        
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    }
}