const { Op } = require('sequelize');

const db = require("../models");
const User = db.User;
const config = require("../config/auth");
const Role = db.Role;
const Tag = db.Tag;
const Category = db.Category;
const Topic = db.Topic;
const Post = db.Post;
const UserPost = db.UsersPosts;
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");

exports.signup = async (req, res) => {
    try {
        User.create({
            username: req.body.username,
            email: req.body.email,
            password: bcrypt.hashSync(req.body.password, 8),
            role: 1
        })
        .then(user => {
            return res.status(200).send({
                status: "success",
                message: `User ${user.username} has been created.`,
                user: user
            })
        })
        .catch(err => {
            return res.status(500).send({ status: "error", message: err.message });
        });
          
    } catch(error) {
        res.status(500).send({status: "error", message: error})
    }
};

exports.signin = async (req, res) => {
    try {
       await User.findOne(
           { where: {username: req.body.username }},
        )
       .then(async user => {
           if(!user) {
               return res.status(404).send({ status: "error", accessToken: null, message: "User not found."})
           }
           const passwordIsValid = bcrypt.compareSync(
               req.body.password,
               user.password
           );
        
           if(!passwordIsValid) {
               return res.status(401).send({
                   status: "error",
                   accessToken: null,
                   message: "Invalid password."
               })
           }

           const token = jwt.sign({id: user.id}, config.secret, {
               expiresIn: 86400
           })

           let role = await Role.findOne({
               where: { id: user.role }
           })
           res.status(200).send({
               status: 'success',
               user: {
                   username: user.username,
                   email: user.email,
                   accessToken: token,
                   role: role,
                   id: user.id
               }
           })
       }).catch(error => {
           res.status(500).send({status: "error", message: error.message})
       })
    } catch(error) {
        res.status(500).send({status: "error", message: error.message})
    }
}

exports.findAll = async (req, res) => {
    try {
        await User.findAll()
        .then(users => {
            return res.status(200).send({
                status: "success",
                message: "All users have been retrieved.",
                users: users
            })
        })
        .catch(err => {
            return res.status(500).send({ status: "error", message: err.message})
        })
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    }
}

exports.findOne = async (req, res) => {
    try {
        const id = req.params.id

        await User.findByPk(id, {
            include: [
                { model: Role },
                { model: Tag },
                { model: Category },
                { model: Topic },
                { model: Post, as: 'writedPosts' },
                { model: UserPost, as: 'followedPosts', attributes: ['id'], include: Post }
            ],
            attributes: [
                "id",
                "username", 
                "email", 
                "password", 
            ]                
        })
        .then(async user => {
            let suggestedTopics = [];
            let findTopicsSuggestions = [];

            for (let index = 0; index < user.Categories.length; index++) {
                findTopicsSuggestions = await Topic.findAll({
                    where: {category: user.Categories[index].id},
                    include: [{ model: Category }]
                })
                suggestedTopics.push(...findTopicsSuggestions)
            }

            let suggestedPosts = [];
            let findPostsSuggestions = [];

            for (let index = 0; index < user.Topics.length; index++) {
                findPostsSuggestions = await Post.findAll({
                    where: {topic: user.Topics[index].id},
                    include: [{ model: Topic}]
                })
                suggestedPosts.push(...findPostsSuggestions)
            }

            if(!user) {
                return res.status(404).send({
                    status: "error",
                    message: "The user was not found.",
                    user: null
                })
            } else {
                return res.status(200).send({
                    status: "success",
                    message: "The user was found.",
                    user: [user, { suggestions: {suggestedTopics, suggestedPosts}}]
                })
            }
        })
    } catch(error) {
        res.status(500).send({status: "error", message: error.message})
    }
}

exports.update = async (req, res) => {
    try {
        const id = req.params.id;

        User.update(req.body, {
            where: {id: id}
        })
        .then(async () => {
            await User.findByPk(id)
            .then(updated => {
                return res.status(200).send({
                    status: "success",
                    message: "The user was updated.",
                    user: updated
                })
            })
        })
        .catch(error => {
            return res.status(500).send({
                status: "error",
                message: "The user has not been updated. | " + error,
                post: null
            })
        })
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    }
}

exports.updateRole = async (req, res) => {
    try {
        const id = req.params.id;

        User.update(req.body, {
            where: {id: id}
        })
        .then(async () => {
            await User.findByPk(id)
            .then(async updated => {

                let new_role = await Role.findByPk(req.body.role);

                return res.status(200).send({
                    status: "success",
                    message: "The role of this user was updated.",
                    user: {
                        username: updated.username,
                        email: updated.email,
                        role: new_role
                    }
                })
            })
        })
        .catch(error => {
            return res.status(500).send({
                status: "error",
                message: "The user has not been updated. | " + error,
                post: null
            })
        })
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    }
}

exports.delete = async (req, res) => {
    try {
        const id = req.params.id;
    
        if(!id) {
            return res.status(404).send({
                status: "error",
                message: `The user ${id} was not found.`
            })
        }

        User.destroy({
            where: { id: id }
        })
        .then(async () => {
            return res.status(200).send({
                status: "success",
                message: "The user was deleted."
            })
        })
        .catch(error => {
            return res.status(500).send({
                status: "error",
                message: "The user was not deleted. | " + error,
                user: null
            })
        })
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    }
}
