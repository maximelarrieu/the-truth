const db = require("../models");
const Tag = db.Tag;
const User = db.User;

exports.findAll = async (req, res) => {
    try {
        await Tag.findAll()
        .then(tags => {
            return res.status(200).send({
                status: 'success',
                message: "All tags have been retrieved.",
                tags: tags
            })
        })
        .catch(err => {
            return res.status(500).send({ status: "error", message: err.message})
        })
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    }
}

exports.findOne = async (req, res) => {
    try {
        const id = req.params.id;

        await Tag.findByPk(id)
        .then(async tag => {
            if(!tag) {
                return res.status(404).send({
                    status: 'error',
                    message: "The tag was not found.",
                    tag: null
                })
            } else {
                return res.status(200).send({
                    status: 'success',
                    message: "The tag was found.",
                    tag: tag
                })
            }
        })
        .catch(error => {
            res.status(500).send({
                status: "error",
                message: "Error retrieving Tag with id=" + id + " | " + error
            });
        })
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    }
}

exports.create = async (req, res) => {
    try {
        if (!req.body.name) {
            return res.status(400).send({
                status: "error",
                message: "Content can not be empty."
            })
        }

        const tag = {
            name: req.body.name,
            createdAt: new Date(),
            updatedAt: new Date(),
        }

        await Tag.create(tag)
        .then(tag => {
            return res.status(200).send({
                status: "success",
                message: "The tag has been created.",
                tag: tag
            })
        })
        .catch(error => {
            return res.status(500).send({
                status: "error",
                message: "The tag has not been created. | " + error,
                post: null
            })
        })
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    }
}

exports.update = async (req, res) => {
    try {
        const id = req.params.id

        Tag.update(req.body, {
            where: {id:id}
        })
        .then(async () => {
            await Tag.findByPk(id)
            .then(updated => {
                return res.status(200).send({
                    status: "error",
                    message: "The tag was updated.",
                    tag: updated
                })
            })
        })
        .catch(error => {
            return res.status(500).send({
                status: "error",
                message: "The tag has not been updated. | " + error,
                post: null
            })
        })
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    }
}

exports.delete = async (req, res) => {
    try {
        const id = req.params.id;
        
        if(!id) {
            return res.status(404).send({
                status: "error",
                message: `The tag ${id} was not found.`
            })
        }

        Tag.destroy({
            where: { id: id }
        })
        .then(async () => {
            return res.status(200).send({
                status: "success",
                message: "The tag was deleted."
            })
        })
        .catch(error => {
            return res.status(500).send({
                status: "error",
                message: "The tag was not deleted. | " + error,
                tag: null
            })
        }) 
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    }
}

exports.addUser = async (req, res) => {
    try {
        const id = req.params.id;

        await Tag.findByPk(id)
            .then(async tag => {
                if(!tag) {
                    return res.status(404).send({
                        status: "error", 
                        message: `The tag was not found.`,
                        tag: null
                    })
                }
                await User.findByPk(req.userId)
                    .then(user => {
                        console.log("USER", user)
                        if(!user) {
                            return res.status(404).send({
                                status: "error", 
                                message: `The user was not found.`,
                                user: null
                            })
                        }

                        user.addTag(tag);
                        
                        return res.status(200).send({
                            status: "success",
                            message: "You followed the tag.",
                            tag: tag
                        })                     
                    })
                    .catch(err => {
                        return res.status(500).send({ status: "error", message: err.message})
                    })
            })
            .catch(err => {
                return res.status(500).send({ status: "error", message: err.message})
            })
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    } 
}

exports.removeUser = async (req, res) => {
    try {
        const id = req.params.id;

        await Tag.findByPk(id)
            .then(async tag => {
                if(!tag) {
                    return res.status(404).send({
                        status: "error", 
                        message: `The tag was not found.`,
                        tag: null
                    })
                }
                await User.findByPk(req.userId)
                    .then(user => {
                        if(!user) {
                            return res.status(404).send({
                                status: "error", 
                                message: `The user was not found.`,
                                user: null
                            })
                        }

                        user.removeTag(tag);
                        
                        return res.status(200).send({
                            status: "success",
                            message: "You unfollowed the tag.",
                            tag: tag
                        })                     
                    })
                    .catch(err => {
                        return res.status(500).send({ status: "error", message: err.message})
                    })
            })
            .catch(err => {
                return res.status(500).send({ status: "error", message: err.message})
            })
    } catch(error) {
        return res.status(500).send({ status: "error", message: error.message })
    } 
}
