#!/bin/bash
npx sequelize-cli db:create
npx sequelize-cli db:migrate
npx sequelize-cli db:seed --seed 20211210084535-demo-roles.js
npx sequelize-cli db:seed --seed 20211209154720-demo-users.js
npx sequelize-cli db:seed --seed 20211210095725-demo-categories.js
npx sequelize-cli db:seed --seed 20211210095822-demo-topics.js
npx sequelize-cli db:seed --seed 20211209154833-demo-posts.js
npx sequelize-cli db:seed --seed 20211210093822-demo-tags.js
npx sequelize-cli db:seed --seed 20211210093551-demo-comments.js
npm start
