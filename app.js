const express = require('express');
const { Sequelize } = require('sequelize');
const db = require('./models');
const config = require('./config/config.json')
const routerUser = require('./routes/api/users.routes');
const routerCategories = require('./routes/api/categories.routes');
const routerComments = require('./routes/api/comments.routes');
const routerNotifications = require('./routes/api/notifications');
const routerPosts = require('./routes/api/posts.routes');
const routerRoles = require('./routes/api/roles.routes');
const routerTopics = require('./routes/api/topics.routes');
const routerTags = require('./routes/api/tags.routes');
const swaggerUI = require("swagger-ui-express");
const swaggerJsDoc = require("swagger-jsdoc");

const sequelize = new Sequelize(config.database, config.username, config.password, {
	host: config.host,
	dialect: config.dialect
});

/**
 * options pour le swagger
 */
 const swaggerSpec = swaggerJsDoc({
	swaggerDefinition: {
		openapi: '3.0.1',
		info: {
		  title: 'The truth - API',
		  version: '1.0.0',
		  description: 'API complète de gestion de forum. Ici nous vous présentons notre projet de forum complotiste où la liberté d\'expression et le droit à la vérité est de vigueur.'
		},
		basePath: '/',
		components: {
		  securitySchemes: {
			bearerAuth: {
			  type: 'http',
			  scheme: 'bearer',
			  bearerFormat: 'JWT',
			}
		  }
		},
		security: [{
		  bearerAuth: []
		}]
	  },
	apis: ["./routes/api/*.js"],
  });
  
const app = express();

app.use("/documentation", swaggerUI.serve, swaggerUI.setup(swaggerSpec));

app.use(express.json());

app.get('/', (req, res) => {
	res.send('Hello World!');
});
app.use('/api/users', routerUser);
app.use('/api/categories', routerCategories);
app.use('/api/comments', routerComments);
app.use('/api/notifications', routerNotifications);
app.use('/api/posts', routerPosts);
app.use('/api/roles', routerRoles);
app.use('/api/topics', routerTopics);
app.use('/api/tags', routerTags);

try {
	sequelize.authenticate();
	console.log("Connection has been established successfully with database.")
} catch(err) {
	console.log("Unable to connect to the database.")
}

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

db.sequelize.sync();

module.exports = app;
