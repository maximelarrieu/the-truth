const db = require("../models");
const User = db.User;

checkDuplicateUsernameOrEmail = (req, res, next) => {
  const { username, email, password } = req.body

  if(!username || !email || !password) {
    return res.status(400).send({
      status: "error",
      message: "All inputs are required."
    })
  }

  User.findOne({
    where: {
      username: req.body.username
    }
  }).then(user => {
    if (user) {
      res.status(400).send({
        status: "error",
        message: "Username is already in use!"
      });
      return;
    }

    User.findOne({
      where: {
        email: req.body.email
      }
    }).then(user => {
      if (user) {
        res.status(400).send({
          status: "error",
          message: "Email is already in use!"
        });
        return;
      }

      next();
    });
  });
};

const verifyRegister = {
  checkDuplicateUsernameOrEmail: checkDuplicateUsernameOrEmail
};

module.exports = verifyRegister;