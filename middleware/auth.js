const jwt = require('jsonwebtoken');
const config = require('../config/auth');
const db = require('../models');
const User = db.User;
const Role = db.Role;
const Post = db.Post;
const Comment = db.Comment;

verifyToken = (req, res, next) => {
    let token = req.headers['authorization'];
    token = token.split(" ").pop();
    if(!token) {
        return res.status(403).send({
            status: "error",
            message: 'No token provided'
        })
    }

    jwt.verify(token, config.secret, (err, decoded) => {
        if(err) {
            return res.status(401).send({
                status: "error",
                message: 'Unauthorized because you are not connected.'
            })
        }
        req.userId = decoded.id;
        next();
    })
}

isAdminOrModerator = async (req, res, next) => {
    await User.findByPk(req.userId).then(async user => {
        let role = await Role.findOne({
            where: { id: user.role }
        })
        if(role.name === "administrator" || role.name === "moderator") {
            next();
            return;
        }

        return res.status(403).send({
            status: "error",
            message: "Administrator or moderator role is required."
        })
    })
}

isAdmin = async (req, res, next) => {
    await User.findByPk(req.userId).then(async user => {
        let role = await Role.findOne({
            where: { id: user.role }
        })
        if(role.name === "administrator") {
            next();
            return;
        }

        return res.status(403).send({
            status: "error",
            message: "Administrator role is required."
        })
    })
}

isModerator = async (req, res, next) => {
    await User.findByPk(req.userId).then(async user => {
        let role = await Role.findOne({
            where: { id: user.role }
        })
        if(role.name === "moderator") {
            next();
            return;
        }

        return res.status(403).send({
            status: "error",
            message: "Moderator role is required."
        })
    })
}

isVisitor = async (req, res, next) => {
    await User.findByPk(req.userId).then(async user => {
        let role = await Role.findOne({
            where: { id: user.role }
        })
        if(role.name === "visitor") {
            next();
            return;
        }

        return res.status(403).send({
            status: "error",
            message: "Visitor role is required."
        })
    })
}

isAuthor = async (req, res, next) => {
    const id = req.params.id;
    const url = req.baseUrl;
    await User.findByPk(req.userId).then(async user => {
        let post = await Post.findByPk(id);
        let comment = await Comment.findByPk(id);

        if(url.includes('posts')) {
            if(!post) {
                return res.status(404).send({
                    status: "error",
                    message: `The post ${id} was not found.`
                })
            }

            if(post.author === user.id) {
                next();
                return;
            } else {
                return res.status(403).send({
                    status: "error",
                    message: "Unautorized because you are not the post author."
                })
            }
        } else if(url.includes('comments')) {
            if(!comment) {
                return res.status(404).send({
                    status: "error",
                    message: `The comment ${id} was not found.`
                }) 
            }
            if(comment.author === user.id) {
                next();
                return;
            } else res.status(403).send({
                status: "error",
                message: "Unauthorized because you are not the comment author."
            })
        }
    })
}

const authJwt = {
    verifyToken: verifyToken,
    isAdminOrModerator: isAdminOrModerator,
    isAdmin: isAdmin,
    isModerator: isModerator,
    isVisitor: isVisitor,
    isAuthor: isAuthor
}

module.exports = authJwt