'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User.hasMany(models.Post, {
        foreignKey: 'author',
        as: 'writedPosts'
      })
      User.belongsTo(models.Role, {
        foreignKey: 'role'
      })
      User.hasMany(models.Comment, {
        foreignKey: 'author'
      })
      User.hasMany(models.UsersPosts, {
        foreignKey: 'user',
        as: 'followedPosts'
      })
      User.belongsToMany(models.Tag, {
        through: 'followersTags'
      })
      User.belongsToMany(models.Category, {
        through: 'followersCategories'
      })
      User.belongsToMany(models.Topic, {
        through: 'followersTopics'
      })
    }
  };
  User.init({
    username: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    role: DataTypes.INTEGER,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'User',
  });
  return User;
};