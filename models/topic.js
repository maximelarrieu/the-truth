'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Topic extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Topic.hasMany(models.Post, {
        foreignKey: 'topic'
      })
      Topic.belongsTo(models.Category, {
        foreignKey: 'category'
      })
      Topic.belongsToMany(models.User, {
        through: 'followersTopics'
      })
    }
  };
  Topic.init({
    name: DataTypes.STRING,
    category: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Topic',
  });
  return Topic;
};