'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UsersPosts extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      UsersPosts.belongsTo(models.User, {
        foreignKey: 'user'
      }),
      UsersPosts.belongsTo(models.Post, {
        foreignKey: 'post'
      })
    }
  };
  UsersPosts.init({
    user: DataTypes.INTEGER,
    post: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'UsersPosts',
  });
  return UsersPosts;
};