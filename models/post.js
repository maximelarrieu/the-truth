'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Post extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Post.belongsTo(models.User, {
        foreignKey: 'author',
        as: 'writedPosts'
      })
      Post.hasMany(models.Comment, {
        foreignKey: 'post'
      })
      Post.belongsToMany(models.Tag, {
        through: 'PostsTags'
      })
      Post.belongsTo(models.Topic, {
        foreignKey: 'topic'
      })
      Post.hasMany(models.UsersPosts, {
        foreignKey: 'post',
        as: 'followedPosts'
      })
    }
  };
  Post.init({
    title: DataTypes.STRING,
    content: DataTypes.TEXT,
    author: DataTypes.INTEGER,
    topic: DataTypes.INTEGER,
    status: DataTypes.BOOLEAN,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
    closedAt: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'Post',
  });
  return Post;
};