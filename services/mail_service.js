const nodemailer = require('nodemailer');
const mailTheTruth = 'the.truth.official.noreply@gmail.com';
const mailPwd = 'Motdepasse1';
const transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    auth: {
      user: mailTheTruth,
      pass: mailPwd
    }
});

/**
 * classe de service contenant les fonctions liées aux mails
 */
class MailService {
    /**
     * fonction d'envoi de mail
     * @param {*} to destinataire
     * @param {*} subject en-tête du mail
     * @param {*} text contenu du mail
     */
    sendMail (to, subject, text){
        try {
            const mailData = {
                from: "The Truth",
                to: to,
                subject: subject,
                text: text
            };
            transporter.sendMail(mailData, (error, info) => {
                if (error) {
                    return Error;
                } else {
                    return true;
                }

            });
        } catch (error) {
            console.log(error)
        }    
    }
}
module.exports = MailService;
